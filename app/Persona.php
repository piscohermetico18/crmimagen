<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model {

    protected $table = 'persona';
  //    protected $dateFormat = 'd/m/Y';
    //  protected $dates = ['fec_inicio_junta_directiva','fec_fin_junta_directiva','fec_fundacion'];
   // protected $dates = ['fec_aniversario'];
    protected $fillable = ['nombre_completo', 'telefono_fijo', 'telefono_celular', 'email'];

}
