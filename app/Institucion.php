<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institucion extends Model {

    protected $table = 'institucion';
  //    protected $dateFormat = 'd/m/Y';
    //  protected $dates = ['fec_inicio_junta_directiva','fec_fin_junta_directiva','fec_fundacion'];
    protected $dates = ['fec_aniversario'];
    protected $fillable = ['codigo_modular', 'nombre', 'categoria_institucion_id', 'direccion', 'ubigeo', 'fec_aniversario', 'pagina_web', 'facebook', 'tipo_institucion_id', 'categoria_oll', 'bachiller_inter', 'asociacion', 'costo_inscripcion', 'costo_matricula', 'costo_mensualidad', 'estudiantes_quinto', 'estudiantes_cuarto', 'estudiantes_tercero'];

}
