<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InstitucionRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'categoria_institucion_id' => 'required|not_in:0',
            //'codigo_modular' => 'required',
            'nombre' => 'required',
            'estudiantes_tercero'=>'integer',
            'estudiantes_cuarto'=>'integer',
            'estudiantes_quinto'=>'integer'
            
        ];
    }

    public function messages() {
        return [
         //   'codigo_modular.required' => 'Ingrese código modular',
            'nombre.required' => 'Ingrese nombre de institución',
            'categoria_institucion_id.not_in' => 'Seleccione una categoría',
            'estudiantes_tercero.integer'=>'Estudiantes Tercero, debe de ser número entero',
            'estudiantes_cuarto.integer'=>'Estudiantes Cuarto, debe de ser número entero',
            'estudiantes_quinto.integer'=>'Estudiantes Quinto, debe de ser número entero'
        ];
    }

}
