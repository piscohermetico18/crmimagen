<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\componentes\FormulariosController;
use App\Institucion;
use App\Persona;
use App\Http\Requests\InstitucionRequest;
use Yajra\Datatables\Datatables;

class InstitucionController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $instituciones = DB::table('institucion')
                ->select('id', 'codigo_modular', 'nombre')
                ->orderBy('id', 'desc')
                ->get();

        //  dd($instituciones);
        return view('instituciones.index', compact('instituciones'));
    }

    public function datatableInstitucion(Request $request) {

        $instituciones = DB::table('institucion')
                ->select('id', 'codigo_modular', 'nombre', 'direccion', 'dpto_prov_dist')
                ->orderBy('id', 'desc')
                ->get();

        return Datatables::of($instituciones, $request)
                        ->addColumn('acciones', function ($data) {

                            $retorno = ' <a href="' . Route('institucion.edit', ['id' => $data->id]) . '" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a> ';

                            return $retorno;
                        })
                        ->rawColumns(['acciones'])
                        ->make(true);
    }

    public function create(Request $request) {
        $formularios = new FormulariosController();
        $departamentos = DB::select("select distinct codubigeo,SUBSTRING(codubigeo, 1, 2) as id, nombre as name  from ubigeo where SUBSTRING(codubigeo, 3, 4)='0000' order by codubigeo");
        $categoriaInstitucion = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_categoria'");
        $tipoInstitucion = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_tipo'");
        $categoriaOll = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_categoria_oll'");
        $cargos = DB::select("select codigo as id,  nombre as name from generica where tipo='contacto_cargo'");
        $bachillerInter = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_bachiller_inter'");

        $datosCategoriaInstitucion = $formularios->datosSelect($categoriaInstitucion);
        $datosTipoInstitucion = $formularios->datosSelect($tipoInstitucion);
        $datosCategoriaOll = $formularios->datosSelect($categoriaOll);
        $datosCargos = $formularios->datosSelect($cargos);
        $datosBachillerInter = $formularios->datosSelect($bachillerInter);
        $datosDepartamentos = $formularios->datosSelect($departamentos);

        return view('instituciones.create', compact('datosDepartamentos', 'datosCategoriaInstitucion', 'datosCategoriaOll', 'datosTipoInstitucion', 'datosBachillerInter', 'datosCargos', 'datosCategoriaInstitucion'));
    }

    public function store(InstitucionRequest $request) {
        try {
            $institucion = Institucion::where([
                            ['categoria_institucion_id', '=', $request->categoria_institucion_id],
                            ['nombre', '=', $request->nombre]
                    ])->first();
            if (count($institucion) > 0) {
                return back()->withInput()
                                ->with('danger', 'Error: Este nombre de institucion ya se encuentra registrado');
            }
            $institucion = Institucion::where([
                            ['codigo_modular', '=', $request->codigo_modular],
                            ['codigo_modular', '<>', null]
                    ])->first();
            if (count($institucion) > 0) {
                return back()->withInput()
                                ->with('danger', 'Error: Código Modular ya existe ');
            }

            if (!is_null($request->fec_aniversario)) {
                $fec_aniversario = \DateTime::createFromFormat('d/m/Y', $request->fec_aniversario);
                $fec_aniversario = $fec_aniversario->format('Y-m-d');
                $request->request->add(['fec_aniversario' => $fec_aniversario]);
            }

            Institucion::create($request->all());
            return redirect()->route('instituciones')
                            ->with('success', 'Institución registrada satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withInput()
                            ->with('danger', $exception->getMessage());
        }
    }

    public function edit($id) {
        $institucion = Institucion::findOrFail($id);
        $formularios = new FormulariosController();
        $departamentos = DB::select("select distinct codubigeo,SUBSTRING(codubigeo, 1, 2) as id, nombre as name  from ubigeo where SUBSTRING(codubigeo, 3, 4)='0000' order by codubigeo");
        $categoriaInstitucion = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_categoria'");
        $tipoInstitucion = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_tipo'");
        $categoriaOll = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_categoria_oll'");
        $cargos = DB::select("select codigo as id,  nombre as name from generica where tipo='contacto_cargo'");
        $bachillerInter = DB::select("select codigo as id,  nombre as name from generica where tipo='institucion_bachiller_inter'");

        $datosCategoriaInstitucion = $formularios->datosSelect($categoriaInstitucion);
        $datosTipoInstitucion = $formularios->datosSelect($tipoInstitucion);
        $datosCategoriaOll = $formularios->datosSelect($categoriaOll);
        $datosCargos = $formularios->datosSelect($cargos);
        $datosBachillerInter = $formularios->datosSelect($bachillerInter);
        $datosDepartamentos = $formularios->datosSelect($departamentos);

        return view('instituciones.edit', compact('institucion', 'datosDepartamentos', 'datosCategoriaInstitucion', 'datosCategoriaOll', 'datosTipoInstitucion', 'datosBachillerInter', 'datosCargos', 'datosCategoriaInstitucion'));
    }

    public function update(InstitucionRequest $request) {
        try {

            $institucion = Institucion::findOrFail($request->id);

            if (!is_null($request->fec_aniversario)) {
                $fec_aniversario = \DateTime::createFromFormat('d/m/Y', $request->fec_aniversario);
                $fec_aniversario = $fec_aniversario->format('Y-m-d');
                $request->request->add(['fec_aniversario' => $fec_aniversario]);
            }

            $institucion->update($request->all());
            return redirect()->route('instituciones')
                            ->with('success', 'Institución actualizada satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withInput()
                            ->with('danger', $exception->getMessage());
        }
    }

    public function storeContacto(Request $request) {
        try {
           /* $persona = Persona::where([
                            ['nombre_completo', '=', $request->nombre_completo]
                    ])->first();
            if (count($persona) > 0) {
                return back()->withInput()
                                ->with('danger', 'Error: Este nombre de persona ya se encuentra registrado');
            }
            $institucion = Institucion::where([
                            ['codigo_modular', '=', $request->codigo_modular],
                            ['codigo_modular', '<>', null]
                    ])->first();
            if (count($institucion) > 0) {
                return back()->withInput()
                                ->with('danger', 'Error: Código Modular ya existe ');
            }

            if (!is_null($request->fec_aniversario)) {
                $fec_aniversario = \DateTime::createFromFormat('d/m/Y', $request->fec_aniversario);
                $fec_aniversario = $fec_aniversario->format('Y-m-d');
                $request->request->add(['fec_aniversario' => $fec_aniversario]);
            }*/

            Persona::create($request->all());
            return redirect()->route('institucion.edit', $request->id_institucion)
                            ->with('success', 'Contacto registrado satisfactoriamente.');
        } catch (\Illuminate\Database\QueryException $exception) {
            return back()->withInput()
                            ->with('danger', $exception->getMessage());
        }
    }

}
