<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
//use DB;
use App\User;
use Auth;

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
        if (Auth::user()['id'] != 1) {
            return redirect('/home');
        }
    }

    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Auth::user()->id != 1) {
            return redirect('/home');
        }

        $users = User::all();


        return view('auth.index', compact('users'));
    }

    public function create() {

        return view('auth.create');
    }

    public function edit($id) {
        $user = User::findOrFail($id);
        return view('auth.edit', compact('user'));
    }

    protected function store(Request $request) {

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return redirect('/users');
    }

    public function update(Request $request) {
        $user = User::findOrFail($request->id);

        $request->request->add(['password' => bcrypt($request->password)]);
        $user->update($request->all());
        return redirect('/users');
    }

}
