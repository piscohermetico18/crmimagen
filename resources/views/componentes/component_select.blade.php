<div class="form-group">
    <label>{{$label}}</label>
    <div class="input-group">
        <select id="{{$name}}"  name="{{$name}}" readonly=true class="form-control select2">
            <option></option>
            @foreach ($data as $d)
            @if ($d->cod == $seleccionado)
            <option selected="selected" value="{{$d->cod}}">
            <div class="row">
                <div class="col-md-6"> {{$d->cod}} </div>
                <div class="col-md-6"> {{$d->name}} </div>
            </div>
            </option>
            @else
            <option value="{{$d->cod}}">
            <div class="row">
                <div class="col-md-6"> {{$d->cod}} </div>
                <div class="col-md-6"> {{$d->name}} </div>
            </div>
            </option>
            @endif
            @endforeach
        </select>
    </div>
</div>

