<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>CRMIMAGEN - </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RENAOS" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

        <link href="{{ asset('/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

        <link href="{{ asset('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->


        <!-- BEGIN PAGE LEVEL DATAPICKER -->
        <link href="{{ asset('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL DATAPICKER -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('/assets/layouts/layout4/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/layouts/layout4/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('/assets/layouts/layout4/css/custom.min.css') }}" rel="stylesheet" type="text/css" />

        @if (Auth::guest())
        <link href="{{ asset('/assets/pages/css/login.min.css') }}" rel="stylesheet" type="text/css" />


        @endif


        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    @if (Auth::guest())
    <body class="login">
        @else    
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">

        @endif




        <!-- BEGIN HEADER -->

        <!-- BEGIN HEADER INNER -->

        @if (Auth::guest())

        @else    
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner  ">
                <div class="page-logo" >
                    <a href="{{ route('inicio') }}">
                        <img style="margin: 0px;margin-top: 10px;margin-left: 40px;width:140px ; height: 40px ;" src="{{ asset('/img/logo.png') }}" alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions">
                    <div class="btn-group">
                        <a href="{{ route('inicio') }}" class="btn btn-primary btn-sm "  >
                            <span class="hidden-sm hidden-xs">CRM - Imagen Institucional y Comunicaciones <strong> CRMIMAGEN</strong></span>

                        </a>

                    </div>
                </div>
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">

                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">

                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile">
                                        <i class="icon-user"></i>
                                        {{ Auth::user()->name }}  

                                    </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">

                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            <i class="icon-logout"></i> Cerrar sesión
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>


                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>


        </div> 

        @endif


        <!-- END HEADER INNER -->




        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">


            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->

                @if (Auth::guest())
                <!--li><a href="{{ route('login') }}">Inicio de sesión</a></li-->
                <!--li><a href="{{ route('register') }}">Register</a></li-->
                @else    
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                        <li class="nav-item start active open">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Inicio</span>
                                <span class="selected"></span>

                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item start ">
                                    <a href="{{ route('inicio') }}" class="nav-link ">
                                        <i class="icon-bar-chart"></i>
                                        <span class="title">Dasboard</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>



                            </ul>
                        </li>

                        <li class="nav-item start active open">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Registro</span>
                                <span class="selected"></span>

                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item start ">
                                    <a href="" class="nav-link ">
                                        <i class="fa fa-bank"></i>
                                        <span class="title">Eventos</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="{{ route('instituciones') }}" class="nav-link ">
                                        <i class="fa fa-sitemap"></i>
                                        <span class="title">Instituciones</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="" class="nav-link ">
                                        <i class="fa fa-cubes"></i>
                                        <span class="title">Tipo Eventos</span>

                                    </a>
                                </li>


                            </ul>
                        </li>

                        @if (Auth::user()->id==1)
                        <li class="nav-item start active open">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-key"></i>
                                <span class="title">Seguridad</span>
                                <span class="selected"></span>

                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item start">
                                    <a href="{{ route('users.index') }}" class="nav-link ">
                                        <i class="icon-user"></i>
                                        <span class="title">Usuarios</span>
                                        <span class="selected"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        @endif

                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>



                @endif


                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">


                @if (Auth::guest())
                @yield('content')
                @else   
                <div class="page-content" style="padding: 0px 0 0 20px">
                    <div class="m-heading-1 border-green m-bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">
                                    @yield('title')
                                </span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body" >

                            @if(Session::has('success'))
                            <div class="alert alert-block alert-success">
                                <i class=" fa fa-check cool-green "></i>
                                {{ nl2br(Session::get('success')) }}
                            </div>
                            @endif
                            @if(Session::has('warning'))
                            <div class="alert alert-block alert-warning">
                                <i class=" fa fa-check cool-green "></i>
                                {{ nl2br(Session::get('warning')) }}
                            </div>
                            @endif
                            @if(Session::has('danger'))
                            <div class="alert alert-block alert-danger">
                                <i class=" fa fa-check cool-green "></i>
                                {{ nl2br(Session::get('danger')) }}
                            </div>
                            @endif
                            @if(Session::has('info'))
                            <div class="alert alert-block alert-info">
                                <i class=" fa fa-check cool-green "></i>
                                {{ nl2br(Session::get('info')) }}
                            </div>
                            @endif
                            
                            @yield('content')

                        </div>
                    </div>
                </div>
                @endif



            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->

        @if (Auth::guest())
        <!--li><a href="{{ route('login') }}">Inicio de sesión</a></li-->
        <!--li><a href="{{ route('register') }}">Register</a></li-->
        @else  
        <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy;  Registro Nacional de Organizaciones Sindicales - RENAOS | 
                <a target="_blank" href="http://www.cgtp.org.pe/">CGTP</a> 

            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        @endif

        <!-- END FOOTER -->

        <!--[if lt IE 9]>
<script src="/assets/global/plugins/respond.min.js"></script>
<script src="/assets/global/plugins/excanvas.min.js"></script> 
<script src="/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ asset('/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/ladda/spin.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/ladda/ladda.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="{{ asset('/assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('/assets/layouts/layout4/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        @yield('js')
        <script>
                                               $(document).ready(function ()
                                               {
                                                   $('#clickmewow').click(function ()
                                                   {
                                                       $('#radio1003').attr('checked', 'checked');
                                                   });
                                               })
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#tbl_sindicatos').DataTable({
                    columnDefs: [{
                            targets: [0],
                            visible: true,
                            searchable: false
                        },
                    ],
                    order: [[0, "asc"]],
                });
            });
        </script>

    </body>


</html>