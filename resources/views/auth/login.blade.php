@extends('layouts.app')

@section('content')
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="index.html">
        <img style="width:180px ; height: 50px ;" src="{{ asset('/img/logo.png') }}" alt="logo" class="logo-default" /></a>
</div>
<!-- END LOGO -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" method="POST" action="{{ route('login') }}" >
        {{ csrf_field() }}
        <h3 class="form-title font-green">CRMIMAGEN</h3>
        <h5 style="text-align: center;" class="form-title font-green">Imagen Institucional y Comunicaciones</h5>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Ingresa usuario y contraseña </span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Usuario</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="email" name="email" value="{{ old('email') }}"  placeholder="Usuario" name="email" id="email" required autofocus  /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Contraseña</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="password" type="password" class="form-control" name="password" required placeholder="Contraseña"  /> </div>
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
        <div class="form-actions" style="text-align: right;">
            <button type="submit"  class="btn  green uppercase">Iniciar sesión</button>
        </div>
        <div class="create-account">
            <p>
            </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->

</div>
@endsection
