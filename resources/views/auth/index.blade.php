@extends('layouts.app')

@section('title')
<h1>Usuarios <a href="{{ route('user.create') }}" class="btn btn-primary pull-right btn-sm">
      Agregar Nuevo Usuario
    </a></h1>
@endsection
@section('content')
<div class="table">
    <table class="table table-bordered table-striped table-hover" id="tbl_users">
        <thead>
            <tr>
                <th>ID</th><th>Nombre</th><th>Email</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name}}</td>
                <td>{{ $item->email }}</td>
                <td>
                      <a href="{{Route('user.edit',[ 'id' => $item->id])}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a> 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>




@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl_users').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
            
          "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        }
        );
    });
</script>
@endsection