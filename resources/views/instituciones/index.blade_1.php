@extends('layouts.app')

@section('title')
<h1>{{ $tipo_organizacion_plural  }} <a href="{{Route('organizacion.create',['tipo_organizacion' => $tipo_organizacion])}}" class="btn btn-primary pull-right btn-sm">
        @if ($tipo_organizacion_id==1)
      Agregar Nuevo
        @else 
        Agregar Nueva 
        @endif  
     {{$tipo_organizacion_name}}
    </a></h1>
@endsection
@section('content')
<div class="table">
    <table class="table table-bordered table-striped table-hover" id="tbl_organizaciones2">
        <thead>
            <tr>
                <th>ID</th><th>Nombre</th><th>Siglas</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($organizaciones as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->nombre_organizacion}}</td>
                <td>{{ $item->siglas_organizacion }}</td>
                <td>
                      <a href="{{Route('organizacion.edit',[ 'id' => $item->id])}}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Editar</a> 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>




@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl_organizaciones2').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
            
          "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        }
        );
    });
</script>
@endsection