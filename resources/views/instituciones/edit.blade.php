@extends('layouts.app')
@section('title')
<h3>Editar Institución</h3>
@endsection

@section('content')

<hr/>


<div class="portlet-body">
    <div class="tabbable-custom ">
        <ul class="nav nav-tabs ">
            <li class="active">
                <a href="#tab_5_1" data-toggle="tab"> Informacion General </a>
            </li>
            <li>
                <a href="#tab_5_2" data-toggle="tab"> Contactos </a>
            </li>
        </ul>

        <div class="tab-content">
            <div  class="tab-pane active" id="tab_5_1">
                @include('fragments.error')


                {!! Form::model($institucion, [
                'method' => 'PUT',
                'route' => ['institucion.update'],
                'class' => 'tab-content',
                'id'=>'formulario'
                ]) !!}

                {!! Form::hidden('id', null, ['class' => 'form-control', 'required'=>'required', 'id' => 'id'  ]) !!}
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Categoria Institución</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('categoria_institucion_id') ? 'has-error' :'' }}">
                                {!! Form::select('categoria_institucion_id', $datosCategoriaInstitucion,null, ['class' => 'form-control']) !!}
                                {!! $errors->first('categoria_institucion_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Código Modular</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('codigo_modular') ? 'has-error' :'' }}">
                                {!! Form::text('codigo_modular', null, ['class' => 'form-control', 'id' => 'codigo_modular' , 'placeholder'=>'Ingrese código modular'  ]) !!}
                                {!! $errors->first('codigo_modular','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-9">
                            <label>Nombre:</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('nombre') ? 'has-error' :'' }}">
                                {!! Form::text('nombre', null, ['class' => 'form-control',  'id' => 'nombre' , 'placeholder'=>'Ingrese nombre de la institución'  ]) !!}
                                {!! $errors->first('nombre','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label>Direccion:</label>
                            <div class="input-group col-md-12 ">
                                {!! Form::text('direccion', null, ['class' => 'form-control', 'id' => 'direccion' , 'placeholder'=>'Ingrese dirección de la institución'  ]) !!}

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label>Departamento:</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('departamento_id') ? 'has-error' :'' }}">
                                {!! Form::select('departamento_id', $datosDepartamentos,substr($institucion->ubigeo, 0, 2), ['class' => 'form-control', 'id' => 'departamento_id']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Provincia:</label>
                            {!! Form::select('provincia_id', ['seleccionar'],'1602', ['class' => 'form-control', 'id' => 'provincia_id']) !!}
                        </div>
                        <div class="col-md-4">
                            <label>Distrito:</label>
                            {!! Form::select('ubigeo', ['seleccionar'],null, ['class' => 'form-control', 'id' => 'ubigeo']) !!}
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-4">
                            <label>Fecha Aniversario:</label>
                            <div class="input-group col-md-12">
                                <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                    {!! Form::text('fec_aniversario', (empty($institucion->fec_aniversario) ? null : $institucion->fec_aniversario->format('d/m/Y'))  , ['class' => 'form-control', 'id' => 'fec_aniversario' ,  'placeholder'=>'dd/mm/aaaa'  ]) !!}
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Página Web:</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('pagina_web') ? 'has-error' :'' }}">
                                {!! Form::text('pagina_web', null, ['class' => 'form-control',  'id' => 'pagina_web' , 'placeholder'=>'Ingrese Página web'  ]) !!}
                                {!! $errors->first('pagina_web','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Facebook:</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('facebook') ? 'has-error' :'' }}">
                                {!! Form::text('facebook', null, ['class' => 'form-control',  'id' => 'facebook' , 'placeholder'=>'Ingrese Facebook'  ]) !!}
                                {!! $errors->first('facebook','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label>Tipo</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('tipo_institucion_id') ? 'has-error' :'' }}">
                                {!! Form::select('tipo_institucion_id', $datosTipoInstitucion,null, ['class' => 'form-control']) !!}
                            </div>

                        </div>
                        <div class="col-md-2">
                            <label>Categoria OII</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('categoria_oll') ? 'has-error' :'' }}">
                                {!! Form::select('categoria_oll', $datosCategoriaOll,null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>Bachiller Inter.</label>
                            <div class="input-group col-md-12 form-group {{ $errors->has('bachiller_inter') ? 'has-error' :'' }}">
                                {!! Form::select('bachiller_inter', $datosBachillerInter,null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Asociación:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('asociacion', null, ['class' => 'form-control', 'id' => 'asociacion' , 'placeholder'=>'Ingrese asociación'  ]) !!}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3">
                            <label>Precios :</label>
                            <div class="input-group form-group {{ $errors->has('costo_inscripcion') ? 'has-error' :'' }}">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Inscripción:
                                </span>
                                {!! Form::text('costo_inscripcion', null, ['class' => 'form-control', 'id' => 'costo_inscripcion' , 'placeholder'=>'Costo'  ]) !!}
                            </div>
                            <div class="input-group form-group {{ $errors->has('costo_matricula') ? 'has-error' :'' }}">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Matricula:
                                </span>
                                {!! Form::text('costo_matricula', null, ['class' => 'form-control', 'id' => 'costo_matricula' , 'placeholder'=>'Costo'  ]) !!}
                            </div>
                            <div class="input-group form-group {{ $errors->has('costo_mensualidad') ? 'has-error' :'' }}">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    Mensualidad:
                                </span>
                                {!! Form::text('costo_mensualidad', null, ['class' => 'form-control', 'id' => 'costo_mensualidad' , 'placeholder'=>'Costo'  ]) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>N° Estudiantes :</label>
                            <div class="input-group form-group {{ $errors->has('estudiantes_quinto') ? 'has-error' :'' }}">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    5to :
                                </span>
                                {!! Form::text('estudiantes_quinto', (empty($institucion->estudiantes_quinto) ? 0 : $institucion->estudiantes_quinto) , ['class' => 'form-control', 'id' => 'estudiantes_quinto' , 'placeholder'=>'Costo'  ]) !!}
                            </div>
                            <div class="input-group form-group {{ $errors->has('estudiantes_cuarto') ? 'has-error' :'' }}">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    4to:
                                </span>
                                {!! Form::text('estudiantes_cuarto', (empty($institucion->estudiantes_cuarto) ? 0 : $institucion->estudiantes_cuarto), ['class' => 'form-control', 'id' => 'estudiantes_cuarto' , 'placeholder'=>'Costo'  ]) !!}
                            </div>
                            <div class="input-group form-group {{ $errors->has('estudiantes_tercero') ? 'has-error' :'' }}">
                                <span class="input-group-addon" style="width: 132px;text-align: right;">
                                    3ro:
                                </span>
                                {!! Form::text('estudiantes_tercero', (empty($institucion->estudiantes_tercero) ? 0 : $institucion->estudiantes_tercero), ['class' => 'form-control', 'id' => 'estudiantes_tercero' , 'placeholder'=>'Costo'  ]) !!}
                            </div>
                        </div>


                    </div>


                    <hr/>
                    <div class="row">
                        @include('componentes.submit_reset_form_update')      
                    </div>

                </div>

                {!! Form::close() !!}

            </div>

              <div  class="tab-pane" id="tab_5_2">
                <div class="portlet-body">
      {!! Form::open(['route' => 'institucion.storecontacto', 'class' => 'tab-content','id'=>'formulario']) !!}
              {!! Form::hidden('id_institucion', $institucion->id, ['class' => 'form-control', 'required'=>'required', 'id' => 'id_institucion'  ]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nombres y Apellidos:</label>
                            <div class="input-group col-md-12">
                                {!! Form::text('nombre_completo', null, ['class' => 'form-control', 'id' => 'nombre_completo' , 'placeholder'=>'Ingrese nombres y apellidos'  ]) !!}

                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Cargo</label>
                            <div class="input-group col-md-12">



                                <div class="input-group col-md-6 form-group {{ $errors->has('cargo_id') ? 'has-error' :'' }}">
                                    {!! Form::select('cargo_id', $datosCargos,null, ['class' => 'form-control']) !!}
                                </div>

                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número fijo
                                </span>
                                {!! Form::text('telefono_fijo', null, ['class' => 'form-control', 'id' => 'telefono_fijo' , 'placeholder'=>'Teléfono Fijo'  ]) !!}

                            </div>

                        </div>
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Número móvil
                                </span>
                                {!! Form::text('telefono_celular', null, ['class' => 'form-control', 'id' => 'telefono_celular' , 'placeholder'=>'Teléfono Móvil'  ]) !!}
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    Email
                                </span>
                                {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email' , 'placeholder'=>'Email'  ]) !!}

                            </div>

                        </div>


                        <div class="col-md-6">
                            <label>&nbsp;</label>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-3">
                                    {!! Form::submit('+ Agregar', ['class' => 'btn btn-primary ', 'id' => 'btn_enviar']) !!}
                                </div>
                            </div>



                        </div>

                    </div>

  {!! Form::close() !!}
                </div>
            </div>















        </div>






    </div>

</div>







@endsection



@section('js')
<script type="text/javascript">

    $(document).ready(function () {

        cargar_provincia('departamento_id', 'provincia_id');
        cargar_distrito('provincia_id', 'ubigeo');
        $('#departamento_id').change(function () {

            cargar_provincia('departamento_id', 'provincia_id');
        });
        $('#provincia_id').change(function () {
            cargar_distrito('provincia_id', 'ubigeo');
        });
    });
    function cargar_provincia(id_change, id_contiene) {

        var departamento_id = $("#" + id_change).val();



        $.post("{{ route('provincia_select')}}",
                {
                    departamento_id: departamento_id,
                    "_token": "{{ csrf_token() }}"
                }).done(function (response) {

            $('#provincia_id').empty();
            $('#provincia_id').append("<option value='0'> Seleccionar </option>");
            for (i = 0; i < response.length; i++) {

                if ('{{old("provincia_id")}}' == '') {

                    if ('{{substr($institucion->ubigeo, 0, 4)}}' == response[i].id) {
                        $('#provincia_id').append("<option selected value='" + response[i].id + "'> " + response[i].name + "</option>");
                    } else {
                        $('#provincia_id').append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
                    }

                } else {
                    if ('{{old("provincia_id")}}' == response[i].id) {
                        $('#provincia_id').append("<option selected value='" + response[i].id + "'> " + response[i].name + "</option>");
                    } else {
                        $('#provincia_id').append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
                    }
                }


            }
            cargar_distrito('provincia_id', 'ubigeo');
        }).fail(function (d) {
            alert('Error');
        });
    }

    function cargar_distrito(id_change, id_contiene) {
        var provincia_id = $("#" + id_change).val();
        $.post("{{ route('distrito_select')}}",
                {
                    provincia_id: provincia_id,
                    "_token": "{{ csrf_token() }}"
                }).done(function (response) {

            $('#ubigeo').empty();
            $('#ubigeo').append("<option value='0'> Seleccionar </option>");
            for (i = 0; i < response.length; i++) {

                if ('{{old("ubigeo")}}' == '') {
                    if ('{{$institucion->ubigeo}}' == response[i].id) {
                        $('#ubigeo').append("<option selected value='" + response[i].id + "'> " + response[i].name + "</option>");
                    } else {
                        $('#ubigeo').append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
                    }

                } else
                {
                    if ('{{old("ubigeo")}}' == response[i].id) {
                        $('#ubigeo').append("<option selected value='" + response[i].id + "'> " + response[i].name + "</option>");
                    } else {
                        $('#ubigeo').append("<option value='" + response[i].id + "'> " + response[i].name + "</option>");
                    }

                }



            }
        }).fail(function (d) {
            alert('Error');
        });
    }










</script>


@endsection