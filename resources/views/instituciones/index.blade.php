@extends('layouts.app')

@section('title')
<h1>Instituciones <a href="{{ route('institucion.create') }}" class="btn btn-primary pull-right btn-sm">

        Agregar Nueva Institución

    </a></h1>
<br>
@endsection
@section('content')

<div class="table">
    <table class="table table-bordered table-striped table-hover" id="tabla">
        <thead>
            <tr>
                <th>Codigo Modular</th><th>Nombre</th><th>Ubigeo</th><th>Direccion</th><th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>




@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        seguimiento();
    });



    function seguimiento() {


        oTable = $('#tabla').DataTable({

            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": true,

            /*"preDrawCallback": function(settings) {
             $('#estudiantes').hide();
             },*/
            'language': {
                "processing": '<b> Cargando...</b>'
            },
            "ajax": {
                "url": "{{ route('instituciones.datatable')}}",
                "type": "GET",
                "data": {
                    "_token": "{{ csrf_token() }}"
                },
                "complete": function (json, type) {
                    $('#tabla').show();
                    if (type == "error") {
                        //   oTableEstudiantes.ajax.reload();
                    }
                },
                "error": function (jqXHR, textStatus, ex) {
                    //  oTableEstudiantes.ajax.reload();
                }
            },
            // "order": [[3, "asc"]],
            "columns": [
                {data: 'codigo_modular', name: 'codigo_modular', visible: true},
                {data: 'nombre', name: 'nombre', visible: true},
                {data: 'dpto_prov_dist', name: 'dpto_prov_dist', visible: true},
                {data: 'direccion', name: 'direccion', visible: true},
                {data: 'acciones', name: 'acciones', visible: true}
            ]
        });


        /*  $('#tabla tbody').on('click', 'tr', function () {
         if ($(this).hasClass('selected')) {
         $(this).removeClass('selected');
         } else {
         oTable.$('tr.selected').removeClass('selected');
         $(this).addClass('selected');
         }
         });
         */
        $('#tabla tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });

        $('#button').click(function () {
            alert(oTable.rows('.selected').data().length + ' row(s) selected');
        });





    }


</script>
@endsection