@extends('layouts.app')

@section('content')

<!-- BEGIN CONTENT BODY -->

<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Dashboard
            <small>Resumen de Eventos</small>
        </h1>
    </div>
    <!-- END PAGE TITLE -->

</div>
<!-- END PAGE HEAD-->

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="{{Route('organizaciones',['tipo_organizacion' => 'sindicatos'])}}">
            <div class="visual">
                <i class="fa fa-bank"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{$count_sindicatos}}">0</span>
                </div>
                <div class="desc"> Sindicatos </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="{{Route('organizaciones',['tipo_organizacion' => 'federaciones'])}}">
            <div class="visual">
                <i class="fa fa-sitemap"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{$count_federaciones}}">0</span> </div>
                <div class="desc"> Federaciones </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="{{Route('organizaciones',['tipo_organizacion' => 'confederaciones'])}}">
            <div class="visual">
                <i class="fa fa-cubes"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{$count_confederaciones}}">0</span>
                </div>
                <div class="desc"> Confederaciones </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="{{Route('organizaciones',['tipo_organizacion' => 'territoriales'])}}">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number"> 
                    <span data-counter="counterup" data-value="{{$count_territoriales}}"></span> </div>
                <div class="desc"> Territoriales </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold uppercase font-dark">Organizaciones</span>
                    <span class="caption-helper">(sindicatos, federaciones, confederaciones y territoriales)</span>
                </div>
                <div class="actions">

                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                </div>
            </div>
            <div class="portlet-body">
           

                    <div class="table">
                        <table class="table table-bordered table-striped table-hover" id="tbl_organizaciones2">
                            <thead>
                                <tr>
                                    <th>ID</th><th>Tipo Organización</th><th>Nombre</th><th>Siglas</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($organizaciones as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->tipo_organizacion }}</td>
                                    <td>{{ $item->nombre_organizacion}}</td>
                                    <td>{{ $item->siglas_organizacion }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


            </div>
        </div>
    </div>

</div>

<!-- END PAGE BASE CONTENT -->

<!-- END CONTENT BODY -->
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl_organizaciones2').DataTable({
            columnDefs: [{
                    targets: [0],
                    visible: true,
                    searchable: false
                },
            ],
            order: [[0, "asc"]],
                   "language": {

    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
            }
        });
    });
</script>
@endsection