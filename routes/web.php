<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});


Route::get('/users', 'Auth\UserController@index')->name('users.index');

Route::get('/user/create', 'Auth\UserController@create')->name('user.create');
Route::post('/user/store', 'Auth\UserController@store')->name('user.store');
Route::get('/user/{id}/edit', 'Auth\UserController@edit')->name('user.edit');
Route::put('/user/update', 'Auth\UserController@update')->name('user.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('inicio');
Route::get('/instituciones', 'InstitucionController@index')->name('instituciones');
Route::get('/instituciones/datatable', 'InstitucionController@datatableInstitucion')->name('instituciones.datatable');
Route::get('/institucion/create', 'InstitucionController@create')->name('institucion.create');
Route::post('/institucion/store', 'InstitucionController@store')->name('institucion.store');
Route::get('/institucion/{id}/edit', 'InstitucionController@edit')->name('institucion.edit');
Route::put('/institucion/update', 'InstitucionController@update')->name('institucion.update');
Route::post('/institucion/storecontacto', 'InstitucionController@storeContacto')->name('institucion.storecontacto');



Route::post('/componentes/select/provincia', 'Componentes\UbigeoController@selectProvincia')->name('provincia_select');
Route::post('/componentes/select/distrito', 'Componentes\UbigeoController@selectDistrito')->name('distrito_select');
