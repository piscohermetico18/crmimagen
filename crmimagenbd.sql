--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.7

-- Started on 2018-03-30 05:31:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 22567)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE estado (
    id integer NOT NULL,
    codigo character varying(10) NOT NULL,
    nombre character varying(50) NOT NULL,
    descripcion text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    updated_ip_address inet,
    created_ip_address inet,
    peso integer DEFAULT 0 NOT NULL
);


ALTER TABLE estado OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 22565)
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estado_id_seq OWNER TO postgres;

--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 186
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estado_id_seq OWNED BY estado.id;


--
-- TOC entry 195 (class 1259 OID 22640)
-- Name: evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE evento (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    descripcion text,
    fecha_inicio timestamp with time zone NOT NULL,
    fecha_fin timestamp with time zone NOT NULL,
    lugar character varying(255),
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    created_ip_address inet,
    updated_ip_address inet,
    id_tipo_evento integer NOT NULL,
    id_institucion integer
);


ALTER TABLE evento OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 22638)
-- Name: evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evento_id_seq OWNER TO postgres;

--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 194
-- Name: evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE evento_id_seq OWNED BY evento.id;


--
-- TOC entry 189 (class 1259 OID 22596)
-- Name: generica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE generica (
    id integer NOT NULL,
    tipo character varying(60) NOT NULL,
    codigo character varying(10) NOT NULL,
    nombre character varying(250) NOT NULL,
    cod_auxiliar character varying(10),
    nombre_auxiliar character varying(100),
    peso integer DEFAULT 0 NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    updated_ip_address inet,
    created_ip_address inet
);


ALTER TABLE generica OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 22594)
-- Name: generica_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE generica_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE generica_id_seq OWNER TO postgres;

--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 188
-- Name: generica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE generica_id_seq OWNED BY generica.id;


--
-- TOC entry 191 (class 1259 OID 22612)
-- Name: institucion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE institucion (
    id integer NOT NULL,
    codigo_modular character varying(10),
    nombre character varying(250) NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    updated_ip_address inet,
    created_ip_address inet,
    nivel_modalidad integer,
    gestion_dependencia integer,
    turno integer
);


ALTER TABLE institucion OWNER TO postgres;

--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 191
-- Name: TABLE institucion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE institucion IS 'Institución o colegio de procedencia';


--
-- TOC entry 190 (class 1259 OID 22610)
-- Name: institucion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE institucion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE institucion_id_seq OWNER TO postgres;

--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 190
-- Name: institucion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE institucion_id_seq OWNED BY institucion.id;


--
-- TOC entry 182 (class 1259 OID 22539)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 22537)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE migrations_id_seq OWNED BY migrations.id;


--
-- TOC entry 185 (class 1259 OID 22558)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE password_resets OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 22664)
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE persona (
    id integer NOT NULL,
    apellido_paterno character varying(100) NOT NULL,
    apellido_materno character varying(100),
    primer_nombre character varying(100) NOT NULL,
    segundo_nombre character varying(100),
    nombre_completo character varying(400) NOT NULL,
    estado_civil character varying(6),
    genero character varying(6),
    telefono_fijo character varying(250),
    telefono_celular character varying(250),
    fecha_nacimiento date NOT NULL,
    tipo_direccion character varying(6),
    direccion character varying(250),
    tipo_doc character varying(6) NOT NULL,
    num_doc character varying(20) NOT NULL,
    email character varying(250) NOT NULL,
    hijo_trabaj character varying(6),
    var_tipo_dir character varying(6) DEFAULT 'TIPDIR'::character varying,
    var_tipo_doc character varying(6) DEFAULT 'TIPIDE'::character varying NOT NULL,
    var_estado_civil character varying(6) DEFAULT 'ESTCIV'::character varying,
    var_genero character varying(6) DEFAULT 'TIPGEN'::character varying,
    var_hijo_trabaj character varying(6) DEFAULT 'HIJOTR'::character varying,
    residencia character varying(250),
    discapacidad boolean DEFAULT false,
    discapacidad_descripcion text,
    comentario text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    updated_ip_address inet,
    created_ip_address inet,
    id_estado integer NOT NULL,
    id_institucion integer NOT NULL,
    anio_termino integer NOT NULL
);


ALTER TABLE persona OWNER TO postgres;

--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN persona.id_estado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN persona.id_estado IS 'Estado actual';


--
-- TOC entry 198 (class 1259 OID 22695)
-- Name: persona_evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE persona_evento (
    id_persona integer NOT NULL,
    id_evento integer NOT NULL,
    confirmado boolean DEFAULT false NOT NULL,
    asistencia boolean DEFAULT false NOT NULL,
    comentario text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    created_ip_address inet,
    updated_ip_address inet
);


ALTER TABLE persona_evento OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 22662)
-- Name: persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE persona_id_seq OWNER TO postgres;

--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 196
-- Name: persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE persona_id_seq OWNED BY persona.id;


--
-- TOC entry 202 (class 1259 OID 22768)
-- Name: personal_institucion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE personal_institucion (
    id integer NOT NULL,
    institucion_id integer,
    persona_id integer,
    cargo_id integer
);


ALTER TABLE personal_institucion OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 22766)
-- Name: personal_institucion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE personal_institucion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personal_institucion_id_seq OWNER TO postgres;

--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 201
-- Name: personal_institucion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE personal_institucion_id_seq OWNED BY personal_institucion.id;


--
-- TOC entry 193 (class 1259 OID 22627)
-- Name: tipo_evento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tipo_evento (
    id integer NOT NULL,
    nombre character varying(250) NOT NULL,
    descripcion text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    created_by integer,
    updated_by integer,
    updated_ip_address inet,
    created_ip_address inet
);


ALTER TABLE tipo_evento OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 22625)
-- Name: tipo_evento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipo_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_evento_id_seq OWNER TO postgres;

--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 192
-- Name: tipo_evento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipo_evento_id_seq OWNED BY tipo_evento.id;


--
-- TOC entry 200 (class 1259 OID 22728)
-- Name: ubigeo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ubigeo (
    id integer NOT NULL,
    codubigeo character varying(20),
    dpto character varying(5),
    prov character varying(5),
    dist character varying(5),
    nombre character varying(600)
);


ALTER TABLE ubigeo OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 22726)
-- Name: ubigeo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ubigeo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ubigeo_id_seq OWNER TO postgres;

--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 199
-- Name: ubigeo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ubigeo_id_seq OWNED BY ubigeo.id;


--
-- TOC entry 184 (class 1259 OID 22547)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 22545)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 2055 (class 2604 OID 22570)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado ALTER COLUMN id SET DEFAULT nextval('estado_id_seq'::regclass);


--
-- TOC entry 2069 (class 2604 OID 22643)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento ALTER COLUMN id SET DEFAULT nextval('evento_id_seq'::regclass);


--
-- TOC entry 2059 (class 2604 OID 22599)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY generica ALTER COLUMN id SET DEFAULT nextval('generica_id_seq'::regclass);


--
-- TOC entry 2063 (class 2604 OID 22615)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institucion ALTER COLUMN id SET DEFAULT nextval('institucion_id_seq'::regclass);


--
-- TOC entry 2053 (class 2604 OID 22542)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations ALTER COLUMN id SET DEFAULT nextval('migrations_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 22667)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona ALTER COLUMN id SET DEFAULT nextval('persona_id_seq'::regclass);


--
-- TOC entry 2086 (class 2604 OID 22771)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personal_institucion ALTER COLUMN id SET DEFAULT nextval('personal_institucion_id_seq'::regclass);


--
-- TOC entry 2066 (class 2604 OID 22630)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_evento ALTER COLUMN id SET DEFAULT nextval('tipo_evento_id_seq'::regclass);


--
-- TOC entry 2085 (class 2604 OID 22731)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ubigeo ALTER COLUMN id SET DEFAULT nextval('ubigeo_id_seq'::regclass);


--
-- TOC entry 2054 (class 2604 OID 22550)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2248 (class 0 OID 22567)
-- Dependencies: 187
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY estado (id, codigo, nombre, descripcion, created_at, updated_at, created_by, updated_by, updated_ip_address, created_ip_address, peso) FROM stdin;
1	001	NUEVO	\N	2018-03-29 18:31:37.666-05	2018-03-29 18:31:37.666-05	\N	\N	\N	\N	0
2	002	INTERESADO 25%	\N	2018-03-29 18:32:07.217-05	2018-03-29 18:32:07.217-05	\N	\N	\N	\N	1
3	003	INTERESADO 50%	\N	2018-03-29 18:32:19.01-05	2018-03-29 18:32:19.01-05	\N	\N	\N	\N	2
4	004	INTERESADO 100%	\N	2018-03-29 18:32:31.785-05	2018-03-29 18:32:31.785-05	\N	\N	\N	\N	3
5	005	NO INTERESADO	\N	2018-03-29 18:32:46.503-05	2018-03-29 18:32:46.503-05	\N	\N	\N	\N	4
\.


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 186
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estado_id_seq', 5, true);


--
-- TOC entry 2256 (class 0 OID 22640)
-- Dependencies: 195
-- Data for Name: evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY evento (id, nombre, descripcion, fecha_inicio, fecha_fin, lugar, created_at, updated_at, created_by, updated_by, created_ip_address, updated_ip_address, id_tipo_evento, id_institucion) FROM stdin;
6	Visita a la Universidad	\N	2018-03-31 00:10:00-05	2018-03-31 11:35:00-05	Honorio Delgado	2018-03-30 00:14:55-05	2018-03-30 00:14:55-05	\N	\N	\N	\N	2	\N
\.


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 194
-- Name: evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('evento_id_seq', 7, true);


--
-- TOC entry 2250 (class 0 OID 22596)
-- Dependencies: 189
-- Data for Name: generica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY generica (id, tipo, codigo, nombre, cod_auxiliar, nombre_auxiliar, peso, created_at, updated_at, created_by, updated_by, updated_ip_address, created_ip_address) FROM stdin;
1	institucion_nivel_modalidad	A1	Inicial - Cuna	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
2	institucion_nivel_modalidad	A2	Inicial - Jardín	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
3	institucion_nivel_modalidad	A3	Inicial - Cuna-jardín	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
4	institucion_nivel_modalidad	A5	Inicial - Programa no escolarizado	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
5	institucion_nivel_modalidad	B0	Primaria	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
6	institucion_nivel_modalidad	C0	Primaria de Adultos	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
7	institucion_nivel_modalidad	D0	Básica Alternativa	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
8	institucion_nivel_modalidad	D1	Básica Alternativa-Inicial e Intermedio	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
9	institucion_nivel_modalidad	D2	Básica Alternativa-Avanzado	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
10	institucion_nivel_modalidad	E0	Básica Especial	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
11	institucion_nivel_modalidad	E1	Básica Especial-Inicial	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
12	institucion_nivel_modalidad	E2	Básica Especial-Primaria	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
13	institucion_nivel_modalidad	F0	Secundaria	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
14	institucion_nivel_modalidad	G0	Secundaria de Adultos	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
15	institucion_nivel_modalidad	K0	Superior Pedagógica	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
16	institucion_nivel_modalidad	L0	Técnico Productiva	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
17	institucion_nivel_modalidad	L1	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
18	institucion_nivel_modalidad	L2	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
19	institucion_nivel_modalidad	L3	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
20	institucion_nivel_modalidad	L4	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
21	institucion_nivel_modalidad	L5	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
22	institucion_nivel_modalidad	L6	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
23	institucion_nivel_modalidad	L7	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
24	institucion_nivel_modalidad	L8	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
25	institucion_nivel_modalidad	L9	Educación Ocupacional	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
26	institucion_nivel_modalidad	M0	Superior Artística	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
27	institucion_nivel_modalidad	T0	Superior Tecnológica	\N	\N	0	2018-03-29 23:00:04.282-05	2018-03-29 23:00:04.282-05	\N	\N	\N	\N
28	institucion_gestion_dependecia	1	Privada - Cooperativa	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
29	institucion_gestion_dependecia	2	Privada - Instituciones Benéficas	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
30	institucion_gestion_dependecia	3	Privada - Parroquial	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
31	institucion_gestion_dependecia	4	Privada - Particular	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
32	institucion_gestion_dependecia	5	Pública - En convenio	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
33	institucion_gestion_dependecia	6	Pública - Municipalidad	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
34	institucion_gestion_dependecia	7	Pública - Otro Sector Público	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
35	institucion_gestion_dependecia	8	Pública - Sector Educación	\N	\N	0	2018-03-29 23:25:50.854-05	2018-03-29 23:25:50.854-05	\N	\N	\N	\N
56	TIPIDE	C	DNI	\N	\N	0	2018-03-30 02:16:33.042-05	2018-03-30 02:16:33.042-05	\N	\N	\N	\N
57	TIPIDE	E	Carné de Extranjería	\N	\N	0	2018-03-30 02:16:49.57-05	2018-03-30 02:16:49.57-05	\N	\N	\N	\N
58	TIPIDE	P	Pasaporte	\N	\N	0	2018-03-30 02:17:00.33-05	2018-03-30 02:17:00.33-05	\N	\N	\N	\N
59	contacto_cargo	1	Director(a)	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
60	contacto_cargo	2	Asistente	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
61	contacto_cargo	3	Consejero(a) Estudiantil	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
62	contacto_cargo	4	Coord. De Estudios	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
41	institucion_categoria_oll	A	A	\N	\N	0	2018-03-30 01:11:24.385-05	2018-03-30 01:11:24.385-05	\N	\N	\N	\N
42	institucion_categoria_oll	B	B	\N	\N	0	2018-03-30 01:11:24.385-05	2018-03-30 01:11:24.385-05	\N	\N	\N	\N
43	institucion_categoria_oll	C	C	\N	\N	0	2018-03-30 01:11:24.385-05	2018-03-30 01:11:24.385-05	\N	\N	\N	\N
36	institucion_categoria	1	Colegio	\N	\N	0	2018-03-29 23:59:35.38-05	2018-03-29 23:59:35.38-05	\N	\N	\N	\N
37	institucion_categoria	2	Academia	\N	\N	0	2018-03-29 23:59:58.399-05	2018-03-29 23:59:58.399-05	\N	\N	\N	\N
44	institucion_tipo	1	Cristiano	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
45	institucion_tipo	2	Laico de Varones	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
46	institucion_tipo	3	Laico Mixto	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
47	institucion_tipo	4	Parroquial	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
48	institucion_tipo	5	Religioso Mixto	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
49	institucion_tipo	6	Religioso Diocesano Mixto	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
50	institucion_tipo	7	Religioso Mujeres	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
51	institucion_tipo	8	Religioso Valores	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
52	institucion_tipo	9	Otros	\N	\N	0	2018-03-30 02:00:58.248-05	2018-03-30 02:00:58.248-05	\N	\N	\N	\N
63	contacto_cargo	5	Coord. Dpto. De Psicología	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
64	contacto_cargo	6	Coord. OBE	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
53	institucion_bachiller_inter	SI	SI	\N	\N	0	2018-03-30 02:04:08.692-05	2018-03-30 02:04:08.692-05	\N	\N	\N	\N
55	institucion_bachiller_inter	NO	NO	\N	\N	0	2018-03-30 02:04:21.314-05	2018-03-30 02:04:21.314-05	\N	\N	\N	\N
65	contacto_cargo	7	Coord. Orientación Vocacional	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
66	contacto_cargo	8	Coord. Tutoría Academica	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
67	contacto_cargo	9	Coord. Academico(a)	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
68	contacto_cargo	10	Coord. Pedagógica	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
69	contacto_cargo	11	Coordinador(a)	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
70	contacto_cargo	12	Coord. De Charla de Orientación	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
71	contacto_cargo	13	Coord. De Secundaria	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
72	contacto_cargo	14	Coord. General	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
73	contacto_cargo	15	Coord. Institucional	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
74	contacto_cargo	16	Director De Desarrollo Personal	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
75	contacto_cargo	17	Director De Estudios	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
76	contacto_cargo	18	Jefe(a) Dpto. Psicología	\N	\N	0	2018-03-30 04:14:51.802-05	2018-03-30 04:14:51.802-05	\N	\N	\N	\N
\.


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 188
-- Name: generica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('generica_id_seq', 76, true);


--
-- TOC entry 2252 (class 0 OID 22612)
-- Dependencies: 191
-- Data for Name: institucion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY institucion (id, codigo_modular, nombre, created_at, updated_at, created_by, updated_by, updated_ip_address, created_ip_address, nivel_modalidad, gestion_dependencia, turno) FROM stdin;
1	0449512	SAGRADOS CORAZONES RECOLETA\r\n	2018-03-29 18:36:57.434-05	2018-03-29 18:36:57.434-05	\N	\N	\N	\N	\N	\N	\N
4	1056829	NUESTRA SEÑORA DEL CARMEN	2018-03-29 18:38:27.735-05	2018-03-29 18:38:27.735-05	\N	\N	\N	\N	\N	\N	\N
3	1723741	INTERNATIONAL CHRISTIAN SCHOOL OF LIMA	2018-03-29 18:38:05.209-05	2018-03-29 18:38:05.209-05	\N	\N	\N	\N	\N	\N	\N
2	1244227	TRILCE DE LA MOLINA	2018-03-29 18:37:16.735-05	2018-03-29 18:37:16.735-05	\N	\N	\N	\N	\N	\N	\N
5	0329169	PIO XII	2018-03-29 18:39:14.835-05	2018-03-29 18:39:14.835-05	\N	\N	\N	\N	\N	\N	\N
6	0305607	CHAMPAGNAT	2018-03-29 18:39:28.704-05	2018-03-29 18:39:28.704-05	\N	\N	\N	\N	\N	\N	\N
\.


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 190
-- Name: institucion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('institucion_id_seq', 6, true);


--
-- TOC entry 2243 (class 0 OID 22539)
-- Dependencies: 182
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
\.


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('migrations_id_seq', 2, true);


--
-- TOC entry 2246 (class 0 OID 22558)
-- Dependencies: 185
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2258 (class 0 OID 22664)
-- Dependencies: 197
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY persona (id, apellido_paterno, apellido_materno, primer_nombre, segundo_nombre, nombre_completo, estado_civil, genero, telefono_fijo, telefono_celular, fecha_nacimiento, tipo_direccion, direccion, tipo_doc, num_doc, email, hijo_trabaj, var_tipo_dir, var_tipo_doc, var_estado_civil, var_genero, var_hijo_trabaj, residencia, discapacidad, discapacidad_descripcion, comentario, created_at, updated_at, created_by, updated_by, updated_ip_address, created_ip_address, id_estado, id_institucion, anio_termino) FROM stdin;
3	PAREDES	DIAZ	JUAN	FRANCISCO	PAREDES DIAZ JUAN FRANCISCO	\N	\N	01 7854123	999777444	1985-10-06	\N	\N	C	43411581	JUAN.PAREDES@UPCH.PE	\N	TIPDIR	TIPIDE	ESTCIV	TIPGEN	HIJOTR	\N	f	\N	\N	2018-03-30 02:36:48.667-05	2018-03-30 02:36:48.667-05	\N	\N	\N	\N	1	1	2015
4	CASIANO	RAMOS	LUIS	JAVIER	CASIANO RAMOS LUIS JAVIER	\N	\N	\N	996258741	1988-03-25	\N	\N	C	44785211	LUIS.CASIANO@UPCH.PE	\N	TIPDIR	TIPIDE	ESTCIV	TIPGEN	HIJOTR	\N	f	\N	\N	2018-03-30 02:38:14.85-05	2018-03-30 02:38:14.85-05	\N	\N	\N	\N	1	2	2017
\.


--
-- TOC entry 2259 (class 0 OID 22695)
-- Dependencies: 198
-- Data for Name: persona_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY persona_evento (id_persona, id_evento, confirmado, asistencia, comentario, created_at, updated_at, created_by, updated_by, created_ip_address, updated_ip_address) FROM stdin;
3	6	t	f	Pendiente confirmar asistencia	2018-03-30 02:48:13.81-05	2018-03-30 02:48:13.81-05	\N	\N	\N	\N
\.


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 196
-- Name: persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('persona_id_seq', 4, true);


--
-- TOC entry 2263 (class 0 OID 22768)
-- Dependencies: 202
-- Data for Name: personal_institucion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY personal_institucion (id, institucion_id, persona_id, cargo_id) FROM stdin;
\.


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 201
-- Name: personal_institucion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('personal_institucion_id_seq', 1, false);


--
-- TOC entry 2254 (class 0 OID 22627)
-- Dependencies: 193
-- Data for Name: tipo_evento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipo_evento (id, nombre, descripcion, created_at, updated_at, created_by, updated_by, updated_ip_address, created_ip_address) FROM stdin;
1	Taller Vivencial	Talleres que simulan la practica profesional de la carrera de los interesados.	2018-03-29 18:29:54.634-05	2018-03-29 21:15:28-05	\N	\N	\N	\N
2	Visita Guiada	Visita a los locales de la ciudad universitaria.	2018-03-29 18:30:03.183-05	2018-03-29 22:01:11-05	\N	\N	\N	\N
3	Charla de Padres	Charlas orientadas a brindar información de las carreras a los padres.	2018-03-29 18:30:21.76-05	2018-03-29 22:38:02-05	\N	\N	\N	\N
\.


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 192
-- Name: tipo_evento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipo_evento_id_seq', 20, true);


--
-- TOC entry 2261 (class 0 OID 22728)
-- Dependencies: 200
-- Data for Name: ubigeo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ubigeo (id, codubigeo, dpto, prov, dist, nombre) FROM stdin;
75	010000	01	00	00	Amazonas
76	010100	01	01	00	Chachapoyas
77	010101	01	01	01	Chachapoyas
78	010102	01	01	02	Asuncion
79	010103	01	01	03	Balsas
80	010104	01	01	04	Cheto
81	010105	01	01	05	Chiliquin
82	010106	01	01	06	Chuquibamba
83	010107	01	01	07	Granada
84	010108	01	01	08	Huancas
85	010109	01	01	09	La Jalca
86	010110	01	01	10	Leimebamba
87	010111	01	01	11	Levanto
88	010112	01	01	12	Magdalena
89	010113	01	01	13	Mariscal Castilla
90	010114	01	01	14	Molinopampa
91	010115	01	01	15	Montevideo
92	010116	01	01	16	Olleros
93	010117	01	01	17	Quinjalca
94	010118	01	01	18	San Francisco de Daguas
95	010119	01	01	19	San Isidro de Maino
96	010120	01	01	20	Soloco
97	010121	01	01	21	Sonche
98	010200	01	02	00	Bagua
99	010201	01	02	01	Bagua
100	010202	01	02	02	Aramango
101	010203	01	02	03	Copallin
102	010204	01	02	04	El Parco
103	010205	01	02	05	Imaza
104	010206	01	02	06	La Peca
105	010300	01	03	00	Bongara
106	010301	01	03	01	Jumbilla
107	010302	01	03	02	Chisquilla
108	010303	01	03	03	Churuja
109	010304	01	03	04	Corosha
110	010305	01	03	05	Cuispes
111	010306	01	03	06	Florida
112	010307	01	03	07	Jazán
113	010308	01	03	08	Recta
114	010309	01	03	09	San Carlos
115	010310	01	03	10	Shipasbamba
116	010311	01	03	11	Valera
117	010312	01	03	12	Yambrasbamba
118	010400	01	04	00	Condorcanqui
119	010401	01	04	01	Nieva
120	010402	01	04	02	El Cenepa
121	010403	01	04	03	Rio Santiago
122	010500	01	05	00	Luya
123	010501	01	05	01	Lamud
124	010502	01	05	02	Camporredondo
125	010503	01	05	03	Cocabamba
126	010504	01	05	04	Colcamar
127	010505	01	05	05	Conila
128	010506	01	05	06	Inguilpata
129	010507	01	05	07	Longuita
130	010508	01	05	08	Lonya Chico
131	010509	01	05	09	Luya
132	010510	01	05	10	Luya Viejo
133	010511	01	05	11	Maria
134	010512	01	05	12	Ocalli
135	010513	01	05	13	Ocumal
136	010514	01	05	14	Pisuquia
137	010515	01	05	15	Providencia
138	010516	01	05	16	San Cristobal
139	010517	01	05	17	San Francisco del Yeso
140	010518	01	05	18	San Jeronimo
141	010519	01	05	19	San Juan de Lopecancha
142	010520	01	05	20	Santa Catalina
143	010521	01	05	21	Santo Tomas
144	010522	01	05	22	Tingo
145	010523	01	05	23	Trita
146	010600	01	06	00	Rodriguez de Mendoza
147	010601	01	06	01	San Nicolas
148	010602	01	06	02	Chirimoto
149	010603	01	06	03	Cochamal
150	010604	01	06	04	Huambo
151	010605	01	06	05	Limabamba
152	010606	01	06	06	Longar
153	010607	01	06	07	Mariscal Benavides
154	010608	01	06	08	Milpuc
155	010609	01	06	09	Omia
156	010610	01	06	10	Santa Rosa
157	010611	01	06	11	Totora
158	010612	01	06	12	Vista Alegre
159	010700	01	07	00	Utcubamba
160	010701	01	07	01	Bagua Grande
161	010702	01	07	02	Cajaruro
162	010703	01	07	03	Cumba
163	010704	01	07	04	El Milagro
164	010705	01	07	05	Jamalca
165	010706	01	07	06	Lonya Grande
166	010707	01	07	07	Yamon
167	020000	02	00	00	Ancash
168	020100	02	01	00	Huaraz
169	020101	02	01	01	Huaraz
170	020102	02	01	02	Cochabamba
171	020103	02	01	03	Colcabamba
172	020104	02	01	04	Huanchay
173	020105	02	01	05	Independencia
174	020106	02	01	06	Jangas
175	020107	02	01	07	La Libertad
176	020108	02	01	08	Olleros
177	020109	02	01	09	Pampas
178	020110	02	01	10	Pariacoto
179	020111	02	01	11	Pira
180	020112	02	01	12	Tarica
181	020200	02	02	00	Aija
182	020201	02	02	01	Aija
183	020202	02	02	02	Coris
184	020203	02	02	03	Huacllan
185	020204	02	02	04	La Merced
186	020205	02	02	05	Succha
187	020300	02	03	00	Antonio Raymondi
188	020301	02	03	01	Llamellin
189	020302	02	03	02	Aczo
190	020303	02	03	03	Chaccho
191	020304	02	03	04	Chingas
192	020305	02	03	05	Mirgas
193	020306	02	03	06	San Juan de Rontoy
194	020400	02	04	00	Asuncion
195	020401	02	04	01	Chacas
196	020402	02	04	02	Acochaca
197	020500	02	05	00	Bolognesi
198	020501	02	05	01	Chiquian
199	020502	02	05	02	Abelardo Pardo Lezameta
200	020503	02	05	03	Antonio Raymondi
201	020504	02	05	04	Aquia
202	020505	02	05	05	Cajacay
203	020506	02	05	06	Canis
204	020507	02	05	07	Colquioc
205	020508	02	05	08	Huallanca
206	020509	02	05	09	Huasta
207	020510	02	05	10	Huayllacayan
208	020511	02	05	11	La Primavera
209	020512	02	05	12	Mangas
210	020513	02	05	13	Pacllon
211	020514	02	05	14	San Miguel de Corpanqui
212	020515	02	05	15	Ticllos
213	020600	02	06	00	Carhuaz
214	020601	02	06	01	Carhuaz
215	020602	02	06	02	Acopampa
216	020603	02	06	03	Amashca
217	020604	02	06	04	Anta
218	020605	02	06	05	Ataquero
219	020606	02	06	06	Marcara
220	020607	02	06	07	Pariahuanca
221	020608	02	06	08	San Miguel de Aco
222	020609	02	06	09	Shilla
223	020610	02	06	10	Tinco
224	020611	02	06	11	Yungar
225	020700	02	07	00	Carlos Fermin Fitzcarrald
226	020701	02	07	01	San Luis
227	020702	02	07	02	San Nicolas
228	020703	02	07	03	Yauya
229	020800	02	08	00	Casma
230	020801	02	08	01	Casma
231	020802	02	08	02	Buena Vista Alta
232	020803	02	08	03	Comandante Noel
233	020804	02	08	04	Yautan
234	020900	02	09	00	Corongo
235	020901	02	09	01	Corongo
236	020902	02	09	02	Aco
237	020903	02	09	03	Bambas
238	020904	02	09	04	Cusca
239	020905	02	09	05	La Pampa
240	020906	02	09	06	Yanac
241	020907	02	09	07	Yupan
242	021000	02	10	00	Huari
243	021001	02	10	01	Huari
244	021002	02	10	02	Anra
245	021003	02	10	03	Cajay
246	021004	02	10	04	Chavin de Huantar
247	021005	02	10	05	Huacachi
248	021006	02	10	06	Huacchis
249	021007	02	10	07	Huachis
250	021008	02	10	08	Huantar
251	021009	02	10	09	Masin
252	021010	02	10	10	Paucas
253	021011	02	10	11	Ponto
254	021012	02	10	12	Rahuapampa
255	021013	02	10	13	Rapayan
256	021014	02	10	14	San Marcos
257	021015	02	10	15	San Pedro de Chana
258	021016	02	10	16	Uco
259	021100	02	11	00	Huarmey
260	021101	02	11	01	Huarmey
261	021102	02	11	02	Cochapeti
262	021103	02	11	03	Culebras
263	021104	02	11	04	Huayan
264	021105	02	11	05	Malvas
265	021200	02	12	00	Huaylas
266	021201	02	12	01	Caraz
267	021202	02	12	02	Huallanca
268	021203	02	12	03	Huata
269	021204	02	12	04	Huaylas
270	021205	02	12	05	Mato
271	021206	02	12	06	Pamparomas
272	021207	02	12	07	Pueblo Libre
273	021208	02	12	08	Santa Cruz
274	021209	02	12	09	Santo Toribio
275	021210	02	12	10	Yuracmarca
276	021300	02	13	00	Mariscal Luzuriaga
277	021301	02	13	01	Piscobamba
278	021302	02	13	02	Casca
279	021303	02	13	03	Eleazar Guzman Barron
280	021304	02	13	04	Fidel Olivas Escudero
281	021305	02	13	05	Llama
282	021306	02	13	06	Llumpa
283	021307	02	13	07	Lucma
284	021308	02	13	08	Musga
285	021400	02	14	00	Ocros
286	021401	02	14	01	Ocros
287	021402	02	14	02	Acas
288	021403	02	14	03	Cajamarquilla
289	021404	02	14	04	Carhuapampa
290	021405	02	14	05	Cochas
291	021406	02	14	06	Congas
292	021407	02	14	07	Llipa
293	021408	02	14	08	San Cristobal de Rajan
294	021409	02	14	09	San Pedro
295	021410	02	14	10	Santiago de Chilcas
296	021500	02	15	00	Pallasca
297	021501	02	15	01	Cabana
298	021502	02	15	02	Bolognesi
299	021503	02	15	03	Conchucos
300	021504	02	15	04	Huacaschuque
301	021505	02	15	05	Huandoval
302	021506	02	15	06	Lacabamba
303	021507	02	15	07	Llapo
304	021508	02	15	08	Pallasca
305	021509	02	15	09	Pampas
306	021510	02	15	10	Santa Rosa
307	021511	02	15	11	Tauca
308	021600	02	16	00	Pomabamba
309	021601	02	16	01	Pomabamba
310	021602	02	16	02	Huayllan
311	021603	02	16	03	Parobamba
312	021604	02	16	04	Quinuabamba
313	021700	02	17	00	Recuay
314	021701	02	17	01	Recuay
315	021702	02	17	02	Catac
316	021703	02	17	03	Cotaparaco
317	021704	02	17	04	Huayllapampa
318	021705	02	17	05	Llacllin
319	021706	02	17	06	Marca
320	021707	02	17	07	Pampas Chico
321	021708	02	17	08	Pararin
322	021709	02	17	09	Tapacocha
323	021710	02	17	10	Ticapampa
324	021800	02	18	00	Santa
325	021801	02	18	01	Chimbote
326	021802	02	18	02	Caceres del Peru
327	021803	02	18	03	Coishco
328	021804	02	18	04	Macate
329	021805	02	18	05	Moro
330	021806	02	18	06	Nepeña
331	021807	02	18	07	Samanco
332	021808	02	18	08	Santa
333	021809	02	18	09	Nuevo Chimbote
334	021900	02	19	00	Sihuas
335	021901	02	19	01	Sihuas
336	021902	02	19	02	Acobamba
337	021903	02	19	03	Alfonso Ugarte
338	021904	02	19	04	Cashapampa
339	021905	02	19	05	Chingalpo
340	021906	02	19	06	Huayllabamba
341	021907	02	19	07	Quiches
342	021908	02	19	08	Ragash
343	021909	02	19	09	San Juan
344	021910	02	19	10	Sicsibamba
345	022000	02	20	00	Yungay
346	022001	02	20	01	Yungay
347	022002	02	20	02	Cascapara
348	022003	02	20	03	Mancos
349	022004	02	20	04	Matacoto
350	022005	02	20	05	Quillo
351	022006	02	20	06	Ranrahirca
352	022007	02	20	07	Shupluy
353	022008	02	20	08	Yanama
354	030000	03	00	00	Apurimac
355	030100	03	01	00	Abancay
356	030101	03	01	01	Abancay
357	030102	03	01	02	Chacoche
358	030103	03	01	03	Circa
359	030104	03	01	04	Curahuasi
360	030105	03	01	05	Huanipaca
361	030106	03	01	06	Lambrama
362	030107	03	01	07	Pichirhua
363	030108	03	01	08	San Pedro de Cachora
364	030109	03	01	09	Tamburco
365	030200	03	02	00	Andahuaylas
366	030201	03	02	01	Andahuaylas
367	030202	03	02	02	Andarapa
368	030203	03	02	03	Chiara
369	030204	03	02	04	Huancarama
370	030205	03	02	05	Huancaray
371	030206	03	02	06	Huayana
372	030207	03	02	07	Kishuara
373	030208	03	02	08	Pacobamba
374	030209	03	02	09	Pacucha
375	030210	03	02	10	Pampachiri
376	030211	03	02	11	Pomacocha
377	030212	03	02	12	San Antonio de Cachi
378	030213	03	02	13	San Jeronimo
379	030214	03	02	14	San Miguel de Chaccrampa
380	030215	03	02	15	Santa Maria de Chicmo
381	030216	03	02	16	Talavera
382	030217	03	02	17	Tumay Huaraca
383	030218	03	02	18	Turpo
384	030219	03	02	19	Kaquiabamba
385	030300	03	03	00	Antabamba
386	030301	03	03	01	Antabamba
387	030302	03	03	02	El Oro
388	030303	03	03	03	Huaquirca
389	030304	03	03	04	Juan Espinoza Medrano
390	030305	03	03	05	Oropesa
391	030306	03	03	06	Pachaconas
392	030307	03	03	07	Sabaino
393	030400	03	04	00	Aymaraes
394	030401	03	04	01	Chalhuanca
395	030402	03	04	02	Capaya
396	030403	03	04	03	Caraybamba
397	030404	03	04	04	Chapimarca
398	030405	03	04	05	Colcabamba
399	030406	03	04	06	Cotaruse
400	030407	03	04	07	Huayllo
401	030408	03	04	08	Justo Apu Sahuaraura
402	030409	03	04	09	Lucre
403	030410	03	04	10	Pocohuanca
404	030411	03	04	11	San Juan de Chacña
405	030412	03	04	12	Sañayca
406	030413	03	04	13	Soraya
407	030414	03	04	14	Tapairihua
408	030415	03	04	15	Tintay
409	030416	03	04	16	Toraya
410	030417	03	04	17	Yanaca
411	030500	03	05	00	Cotabambas
412	030501	03	05	01	Tambobamba
413	030502	03	05	02	Cotabambas
414	030503	03	05	03	Coyllurqui
415	030504	03	05	04	Haquira
416	030505	03	05	05	Mara
417	030506	03	05	06	Challhuahuacho
418	030600	03	06	00	Chincheros
419	030601	03	06	01	Chincheros
420	030602	03	06	02	Anco-Huallo
421	030603	03	06	03	Cocharcas
422	030604	03	06	04	Huaccana
423	030605	03	06	05	Ocobamba
424	030606	03	06	06	Ongoy
425	030607	03	06	07	Uranmarca
426	030608	03	06	08	Ranracancha
427	030700	03	07	00	Grau
428	030701	03	07	01	Chuquibambilla
429	030702	03	07	02	Curpahuasi
430	030703	03	07	03	Gamarra
431	030704	03	07	04	Huayllati
432	030705	03	07	05	Mamara
433	030706	03	07	06	Micaela Bastidas
434	030707	03	07	07	Pataypampa
435	030708	03	07	08	Progreso
436	030709	03	07	09	San Antonio
437	030710	03	07	10	Santa Rosa
438	030711	03	07	11	Turpay
439	030712	03	07	12	Vilcabamba
440	030713	03	07	13	Virundo
441	030714	03	07	14	Curasco
442	040000	04	00	00	Arequipa
443	040100	04	01	00	Arequipa
444	040101	04	01	01	Arequipa
445	040102	04	01	02	Alto Selva Alegre
446	040103	04	01	03	Cayma
447	040104	04	01	04	Cerro Colorado
448	040105	04	01	05	Characato
449	040106	04	01	06	Chiguata
450	040107	04	01	07	Jacobo Hunter
451	040108	04	01	08	La Joya
452	040109	04	01	09	Mariano Melgar
453	040110	04	01	10	Miraflores
454	040111	04	01	11	Mollebaya
455	040112	04	01	12	Paucarpata
456	040113	04	01	13	Pocsi
457	040114	04	01	14	Polobaya
458	040115	04	01	15	Quequeña
459	040116	04	01	16	Sabandia
460	040117	04	01	17	Sachaca
461	040118	04	01	18	San Juan de Siguas
462	040119	04	01	19	San Juan de Tarucani
463	040120	04	01	20	Santa Isabel de Siguas
464	040121	04	01	21	Santa Rita de Siguas
465	040122	04	01	22	Socabaya
466	040123	04	01	23	Tiabaya
467	040124	04	01	24	Uchumayo
468	040125	04	01	25	Vitor
469	040126	04	01	26	Yanahuara
470	040127	04	01	27	Yarabamba
471	040128	04	01	28	Yura
472	040129	04	01	29	Jose Luis Bustamante y Rivero
473	040200	04	02	00	Camana
474	040201	04	02	01	Camana
475	040202	04	02	02	Jose Maria Quimper
476	040203	04	02	03	Mariano Nicolas Valcarcel
477	040204	04	02	04	Mariscal Caceres
478	040205	04	02	05	Nicolas de Pierola
479	040206	04	02	06	Ocoña
480	040207	04	02	07	Quilca
481	040208	04	02	08	Samuel Pastor
482	040300	04	03	00	Caraveli
483	040301	04	03	01	Caraveli
484	040302	04	03	02	Acari
485	040303	04	03	03	Atico
486	040304	04	03	04	Atiquipa
487	040305	04	03	05	Bella Union
488	040306	04	03	06	Cahuacho
489	040307	04	03	07	Chala
490	040308	04	03	08	Chaparra
491	040309	04	03	09	Huanuhuanu
492	040310	04	03	10	Jaqui
493	040311	04	03	11	Lomas
494	040312	04	03	12	Quicacha
495	040313	04	03	13	Yauca
496	040400	04	04	00	Castilla
497	040401	04	04	01	Aplao
498	040402	04	04	02	Andagua
499	040403	04	04	03	Ayo
500	040404	04	04	04	Chachas
501	040405	04	04	05	Chilcaymarca
502	040406	04	04	06	Choco
503	040407	04	04	07	Huancarqui
504	040408	04	04	08	Machaguay
505	040409	04	04	09	Orcopampa
506	040410	04	04	10	Pampacolca
507	040411	04	04	11	Tipan
508	040412	04	04	12	Uñon
509	040413	04	04	13	Uraca
510	040414	04	04	14	Viraco
511	040500	04	05	00	Caylloma
512	040501	04	05	01	Chivay
513	040502	04	05	02	Achoma
514	040503	04	05	03	Cabanaconde
515	040504	04	05	04	Callalli
516	040505	04	05	05	Caylloma
517	040506	04	05	06	Coporaque
518	040507	04	05	07	Huambo
519	040508	04	05	08	Huanca
520	040509	04	05	09	Ichupampa
521	040510	04	05	10	Lari
522	040511	04	05	11	Lluta
523	040512	04	05	12	Maca
524	040513	04	05	13	Madrigal
525	040514	04	05	14	San Antonio de Chuca
526	040515	04	05	15	Sibayo
527	040516	04	05	16	Tapay
528	040517	04	05	17	Tisco
529	040518	04	05	18	Tuti
530	040519	04	05	19	Yanque
531	040520	04	05	20	Majes
532	040600	04	06	00	Condesuyos
533	040601	04	06	01	Chuquibamba
534	040602	04	06	02	Andaray
535	040603	04	06	03	Cayarani
536	040604	04	06	04	Chichas
537	040605	04	06	05	Iray
538	040606	04	06	06	Rio Grande
539	040607	04	06	07	Salamanca
540	040608	04	06	08	Yanaquihua
541	040700	04	07	00	Islay
542	040701	04	07	01	Mollendo
543	040702	04	07	02	Cocachacra
544	040703	04	07	03	Dean Valdivia
545	040704	04	07	04	Islay
546	040705	04	07	05	Mejia
547	040706	04	07	06	Punta de Bombon
548	040800	04	08	00	La Union
549	040801	04	08	01	Cotahuasi
550	040802	04	08	02	Alca
551	040803	04	08	03	Charcana
552	040804	04	08	04	Huaynacotas
553	040805	04	08	05	Pampamarca
554	040806	04	08	06	Puyca
555	040807	04	08	07	Quechualla
556	040808	04	08	08	Sayla
557	040809	04	08	09	Tauria
558	040810	04	08	10	Tomepampa
559	040811	04	08	11	Toro
560	050000	05	00	00	Ayacucho
561	050100	05	01	00	Huamanga
562	050101	05	01	01	Ayacucho
563	050102	05	01	02	Acocro
564	050103	05	01	03	Acos Vinchos
565	050104	05	01	04	Carmen Alto
566	050105	05	01	05	Chiara
567	050106	05	01	06	Ocros
568	050107	05	01	07	Pacaycasa
569	050108	05	01	08	Quinua
570	050109	05	01	09	San Jose de Ticllas
571	050110	05	01	10	San Juan Bautista
572	050111	05	01	11	Santiago de Pischa
573	050112	05	01	12	Socos
574	050113	05	01	13	Tambillo
575	050114	05	01	14	Vinchos
576	050115	05	01	15	Jesús Nazareno
577	050116	05	01	16	Andrés Avelino Cáceres Dorregay
578	050200	05	02	00	Cangallo
579	050201	05	02	01	Cangallo
580	050202	05	02	02	Chuschi
581	050203	05	02	03	Los Morochucos
582	050204	05	02	04	Maria Parado de Bellido
583	050205	05	02	05	Paras
584	050206	05	02	06	Totos
585	050300	05	03	00	Huanca Sancos
586	050301	05	03	01	Sancos
587	050302	05	03	02	Carapo
588	050303	05	03	03	Sacsamarca
589	050304	05	03	04	Santiago de Lucanamarca
590	050400	05	04	00	Huanta
591	050401	05	04	01	Huanta
592	050402	05	04	02	Ayahuanco
593	050403	05	04	03	Huamanguilla
594	050404	05	04	04	Iguain
595	050405	05	04	05	Luricocha
596	050406	05	04	06	Santillana
597	050407	05	04	07	Sivia
598	050408	05	04	08	Llochegua
599	050409	05	04	09	Canayre
600	050410	05	04	10	Uchuraccay
601	050411	05	04	11	Pucacolpa
602	050500	05	05	00	La Mar
603	050501	05	05	01	San Miguel
604	050502	05	05	02	Anco
605	050503	05	05	03	Ayna
606	050504	05	05	04	Chilcas
607	050505	05	05	05	Chungui
608	050506	05	05	06	Luis Carranza
609	050507	05	05	07	Santa Rosa
610	050508	05	05	08	Tambo
611	050509	05	05	09	Samugari
612	050510	05	05	10	Anchihuay
613	050600	05	06	00	Lucanas
614	050601	05	06	01	Puquio
615	050602	05	06	02	Aucara
616	050603	05	06	03	Cabana
617	050604	05	06	04	Carmen Salcedo
618	050605	05	06	05	Chaviña
619	050606	05	06	06	Chipao
620	050607	05	06	07	Huac-Huas
621	050608	05	06	08	Laramate
622	050609	05	06	09	Leoncio Prado
623	050610	05	06	10	Llauta
624	050611	05	06	11	Lucanas
625	050612	05	06	12	Ocaña
626	050613	05	06	13	Otoca
627	050614	05	06	14	Saisa
628	050615	05	06	15	San Cristobal
629	050616	05	06	16	San Juan
630	050617	05	06	17	San Pedro
631	050618	05	06	18	San Pedro de Palco
632	050619	05	06	19	Sancos
633	050620	05	06	20	Santa Ana de Huaycahuacho
634	050621	05	06	21	Santa Lucia
635	050700	05	07	00	Parinacochas
636	050701	05	07	01	Coracora
637	050702	05	07	02	Chumpi
638	050703	05	07	03	Coronel Castañeda
639	050704	05	07	04	Pacapausa
640	050705	05	07	05	Pullo
641	050706	05	07	06	Puyusca
642	050707	05	07	07	San Francisco de Ravacayco
643	050708	05	07	08	Upahuacho
644	050800	05	08	00	Paucar del Sara Sara
645	050801	05	08	01	Pausa
646	050802	05	08	02	Colta
647	050803	05	08	03	Corculla
648	050804	05	08	04	Lampa
649	050805	05	08	05	Marcabamba
650	050806	05	08	06	Oyolo
651	050807	05	08	07	Pararca
652	050808	05	08	08	San Javier de Alpabamba
653	050809	05	08	09	San Jose de Ushua
654	050810	05	08	10	Sara Sara
655	050900	05	09	00	Sucre
656	050901	05	09	01	Querobamba
657	050902	05	09	02	Belen
658	050903	05	09	03	Chalcos
659	050904	05	09	04	Chilcayoc
660	050905	05	09	05	Huacaña
661	050906	05	09	06	Morcolla
662	050907	05	09	07	Paico
663	050908	05	09	08	San Pedro de Larcay
664	050909	05	09	09	San Salvador de Quije
665	050910	05	09	10	Santiago de Paucaray
666	050911	05	09	11	Soras
667	051000	05	10	00	Victor Fajardo
668	051001	05	10	01	Huancapi
669	051002	05	10	02	Alcamenca
670	051003	05	10	03	Apongo
671	051004	05	10	04	Asquipata
672	051005	05	10	05	Canaria
673	051006	05	10	06	Cayara
674	051007	05	10	07	Colca
675	051008	05	10	08	Huamanquiquia
676	051009	05	10	09	Huancaraylla
677	051010	05	10	10	Huaya
678	051011	05	10	11	Sarhua
679	051012	05	10	12	Vilcanchos
680	051100	05	11	00	Vilcas Huaman
681	051101	05	11	01	Vilcas Huaman
682	051102	05	11	02	Accomarca
683	051103	05	11	03	Carhuanca
684	051104	05	11	04	Concepcion
685	051105	05	11	05	Huambalpa
686	051106	05	11	06	Independencia
687	051107	05	11	07	Saurama
688	051108	05	11	08	Vischongo
689	060000	06	00	00	Cajamarca
690	060100	06	01	00	Cajamarca
691	060101	06	01	01	Cajamarca
692	060102	06	01	02	Asuncion
693	060103	06	01	03	Chetilla
694	060104	06	01	04	Cospan
695	060105	06	01	05	Encañada
696	060106	06	01	06	Jesus
697	060107	06	01	07	Llacanora
698	060108	06	01	08	Los Baños del Inca
699	060109	06	01	09	Magdalena
700	060110	06	01	10	Matara
701	060111	06	01	11	Namora
702	060112	06	01	12	San Juan
703	060200	06	02	00	Cajabamba
704	060201	06	02	01	Cajabamba
705	060202	06	02	02	Cachachi
706	060203	06	02	03	Condebamba
707	060204	06	02	04	Sitacocha
708	060300	06	03	00	Celendin
709	060301	06	03	01	Celendin
710	060302	06	03	02	Chumuch
711	060303	06	03	03	Cortegana
712	060304	06	03	04	Huasmin
713	060305	06	03	05	Jorge Chavez
714	060306	06	03	06	Jose Galvez
715	060307	06	03	07	Miguel Iglesias
716	060308	06	03	08	Oxamarca
717	060309	06	03	09	Sorochuco
718	060310	06	03	10	Sucre
719	060311	06	03	11	Utco
720	060312	06	03	12	La Libertad de Pallan
721	060400	06	04	00	Chota
722	060401	06	04	01	Chota
723	060402	06	04	02	Anguia
724	060403	06	04	03	Chadin
725	060404	06	04	04	Chiguirip
726	060405	06	04	05	Chimban
727	060406	06	04	06	Choropampa
728	060407	06	04	07	Cochabamba
729	060408	06	04	08	Conchan
730	060409	06	04	09	Huambos
731	060410	06	04	10	Lajas
732	060411	06	04	11	Llama
733	060412	06	04	12	Miracosta
734	060413	06	04	13	Paccha
735	060414	06	04	14	Pion
736	060415	06	04	15	Querocoto
737	060416	06	04	16	San Juan de Licupis
738	060417	06	04	17	Tacabamba
739	060418	06	04	18	Tocmoche
740	060419	06	04	19	Chalamarca
741	060500	06	05	00	Contumaza
742	060501	06	05	01	Contumaza
743	060502	06	05	02	Chilete
744	060503	06	05	03	Cupisnique
745	060504	06	05	04	Guzmango
746	060505	06	05	05	San Benito
747	060506	06	05	06	Santa Cruz de Toled
748	060507	06	05	07	Tantarica
749	060508	06	05	08	Yonan
750	060600	06	06	00	Cutervo
751	060601	06	06	01	Cutervo
752	060602	06	06	02	Callayuc
753	060603	06	06	03	Choros
754	060604	06	06	04	Cujillo
755	060605	06	06	05	La Ramada
756	060606	06	06	06	Pimpingos
757	060607	06	06	07	Querocotillo
758	060608	06	06	08	San Andres de Cutervo
759	060609	06	06	09	San Juan de Cutervo
760	060610	06	06	10	San Luis de Lucma
761	060611	06	06	11	Santa Cruz
762	060612	06	06	12	Santo Domingo de la Capilla
763	060613	06	06	13	Santo Tomas
764	060614	06	06	14	Socota
765	060615	06	06	15	Toribio Casanova
766	060700	06	07	00	Hualgayoc
767	060701	06	07	01	Bambamarca
768	060702	06	07	02	Chugur
769	060703	06	07	03	Hualgayoc
770	060800	06	08	00	Jaen
771	060801	06	08	01	Jaen
772	060802	06	08	02	Bellavista
773	060803	06	08	03	Chontali
774	060804	06	08	04	Colasay
775	060805	06	08	05	Huabal
776	060806	06	08	06	Las Pirias
777	060807	06	08	07	Pomahuaca
778	060808	06	08	08	Pucara
779	060809	06	08	09	Sallique
780	060810	06	08	10	San Felipe
781	060811	06	08	11	San Jose del Alto
782	060812	06	08	12	Santa Rosa
783	060900	06	09	00	San Ignacio
784	060901	06	09	01	San Ignacio
785	060902	06	09	02	Chirinos
786	060903	06	09	03	Huarango
787	060904	06	09	04	La Coipa
788	060905	06	09	05	Namballe
789	060906	06	09	06	San Jose de Lourdes
790	060907	06	09	07	Tabaconas
791	061000	06	10	00	San Marcos
792	061001	06	10	01	Pedro Galvez
793	061002	06	10	02	Chancay
794	061003	06	10	03	Eduardo Villanueva
795	061004	06	10	04	Gregorio Pita
796	061005	06	10	05	Ichocan
797	061006	06	10	06	Jose Manuel Quiroz
798	061007	06	10	07	Jose Sabogal
799	061100	06	11	00	San Miguel
800	061101	06	11	01	San Miguel
801	061102	06	11	02	Bolivar
802	061103	06	11	03	Calquis
803	061104	06	11	04	Catilluc
804	061105	06	11	05	El Prado
805	061106	06	11	06	La Florida
806	061107	06	11	07	Llapa
807	061108	06	11	08	Nanchoc
808	061109	06	11	09	Niepos
809	061110	06	11	10	San Gregorio
810	061111	06	11	11	San Silvestre de Cochan
811	061112	06	11	12	Tongod
812	061113	06	11	13	Union Agua Blanca
813	061200	06	12	00	San Pablo
814	061201	06	12	01	San Pablo
815	061202	06	12	02	San Bernardino
816	061203	06	12	03	San Luis
817	061204	06	12	04	Tumbaden
818	061300	06	13	00	Santa Cruz
819	061301	06	13	01	Santa Cruz
820	061302	06	13	02	Andabamba
821	061303	06	13	03	Catache
822	061304	06	13	04	Chancaybaños
823	061305	06	13	05	La Esperanza
824	061306	06	13	06	Ninabamba
825	061307	06	13	07	Pulan
826	061308	06	13	08	Saucepampa
827	061309	06	13	09	Sexi
828	061310	06	13	10	Uticyacu
829	061311	06	13	11	Yauyucan
830	070000	07	00	00	Callao
831	070100	07	01	00	Prov. Const. del Callao
832	070101	07	01	01	Callao
833	070102	07	01	02	Bellavista
834	070103	07	01	03	Carmen de la Legua Reynoso
835	070104	07	01	04	La Perla
836	070105	07	01	05	La Punta
837	070106	07	01	06	Ventanilla
838	070107	07	01	07	Mi Perú
839	080000	08	00	00	Cusco
840	080100	08	01	00	Cusco
841	080101	08	01	01	Cusco
842	080102	08	01	02	Ccorca
843	080103	08	01	03	Poroy
844	080104	08	01	04	San Jeronimo
845	080105	08	01	05	San Sebastian
846	080106	08	01	06	Santiago
847	080107	08	01	07	Saylla
848	080108	08	01	08	Wanchaq
849	080200	08	02	00	Acomayo
850	080201	08	02	01	Acomayo
851	080202	08	02	02	Acopia
852	080203	08	02	03	Acos
853	080204	08	02	04	Mosoc Llacta
854	080205	08	02	05	Pomacanchi
855	080206	08	02	06	Rondocan
856	080207	08	02	07	Sangarara
857	080300	08	03	00	Anta
858	080301	08	03	01	Anta
859	080302	08	03	02	Ancahuasi
860	080303	08	03	03	Cachimayo
861	080304	08	03	04	Chinchaypujio
862	080305	08	03	05	Huarocondo
863	080306	08	03	06	Limatambo
864	080307	08	03	07	Mollepata
865	080308	08	03	08	Pucyura
866	080309	08	03	09	Zurite
867	080400	08	04	00	Calca
868	080401	08	04	01	Calca
869	080402	08	04	02	Coya
870	080403	08	04	03	Lamay
871	080404	08	04	04	Lares
872	080405	08	04	05	Pisac
873	080406	08	04	06	San Salvador
874	080407	08	04	07	Taray
875	080408	08	04	08	Yanatile
876	080500	08	05	00	Canas
877	080501	08	05	01	Yanaoca
878	080502	08	05	02	Checca
879	080503	08	05	03	Kunturkanki
880	080504	08	05	04	Langui
881	080505	08	05	05	Layo
882	080506	08	05	06	Pampamarca
883	080507	08	05	07	Quehue
884	080508	08	05	08	Tupac Amaru
885	080600	08	06	00	Canchis
886	080601	08	06	01	Sicuani
887	080602	08	06	02	Checacupe
888	080603	08	06	03	Combapata
889	080604	08	06	04	Marangani
890	080605	08	06	05	Pitumarca
891	080606	08	06	06	San Pablo
892	080607	08	06	07	San Pedro
893	080608	08	06	08	Tinta
894	080700	08	07	00	Chumbivilcas
895	080701	08	07	01	Santo Tomas
896	080702	08	07	02	Capacmarca
897	080703	08	07	03	Chamaca
898	080704	08	07	04	Colquemarca
899	080705	08	07	05	Livitaca
900	080706	08	07	06	Llusco
901	080707	08	07	07	Quiñota
902	080708	08	07	08	Velille
903	080800	08	08	00	Espinar
904	080801	08	08	01	Espinar
905	080802	08	08	02	Condoroma
906	080803	08	08	03	Coporaque
907	080804	08	08	04	Ocoruro
908	080805	08	08	05	Pallpata
909	080806	08	08	06	Pichigua
910	080807	08	08	07	Suyckutambo
911	080808	08	08	08	Alto Pichigua
912	080900	08	09	00	La Convencion
913	080901	08	09	01	Santa Ana
914	080902	08	09	02	Echarate
915	080903	08	09	03	Huayopata
916	080904	08	09	04	Maranura
917	080905	08	09	05	Ocobamba
918	080906	08	09	06	Quellouno
919	080907	08	09	07	Kimbiri
920	080908	08	09	08	Santa Teresa
921	080909	08	09	09	Vilcabamba
922	080910	08	09	10	Pichari
923	080911	08	09	11	Inkawasi
924	080912	08	09	12	Villa Virgen
925	081000	08	10	00	Paruro
926	081001	08	10	01	Paruro
927	081002	08	10	02	Accha
928	081003	08	10	03	Ccapi
929	081004	08	10	04	Colcha
930	081005	08	10	05	Huanoquite
931	081006	08	10	06	Omacha
932	081007	08	10	07	Paccaritambo
933	081008	08	10	08	Pillpinto
934	081009	08	10	09	Yaurisque
935	081100	08	11	00	Paucartambo
936	081101	08	11	01	Paucartambo
937	081102	08	11	02	Caicay
938	081103	08	11	03	Challabamba
939	081104	08	11	04	Colquepata
940	081105	08	11	05	Huancarani
941	081106	08	11	06	Kosñipata
942	081200	08	12	00	Quispicanchi
943	081201	08	12	01	Urcos
944	081202	08	12	02	Andahuaylillas
945	081203	08	12	03	Camanti
946	081204	08	12	04	Ccarhuayo
947	081205	08	12	05	Ccatca
948	081206	08	12	06	Cusipata
949	081207	08	12	07	Huaro
950	081208	08	12	08	Lucre
951	081209	08	12	09	Marcapata
952	081210	08	12	10	Ocongate
953	081211	08	12	11	Oropesa
954	081212	08	12	12	Quiquijana
955	081300	08	13	00	Urubamba
956	081301	08	13	01	Urubamba
957	081302	08	13	02	Chinchero
958	081303	08	13	03	Huayllabamba
959	081304	08	13	04	Machupicchu
960	081305	08	13	05	Maras
961	081306	08	13	06	Ollantaytambo
962	081307	08	13	07	Yucay
963	090000	09	00	00	Huancavelica
964	090100	09	01	00	Huancavelica
965	090101	09	01	01	Huancavelica
966	090102	09	01	02	Acobambilla
967	090103	09	01	03	Acoria
968	090104	09	01	04	Conayca
969	090105	09	01	05	Cuenca
970	090106	09	01	06	Huachocolpa
971	090107	09	01	07	Huayllahuara
972	090108	09	01	08	Izcuchaca
973	090109	09	01	09	Laria
974	090110	09	01	10	Manta
975	090111	09	01	11	Mariscal Caceres
976	090112	09	01	12	Moya
977	090113	09	01	13	Nuevo Occoro
978	090114	09	01	14	Palca
979	090115	09	01	15	Pilchaca
980	090116	09	01	16	Vilca
981	090117	09	01	17	Yauli
982	090118	09	01	18	Ascensión
983	090119	09	01	19	Huando
984	090200	09	02	00	Acobamba
985	090201	09	02	01	Acobamba
986	090202	09	02	02	Andabamba
987	090203	09	02	03	Anta
988	090204	09	02	04	Caja
989	090205	09	02	05	Marcas
990	090206	09	02	06	Paucara
991	090207	09	02	07	Pomacocha
992	090208	09	02	08	Rosario
993	090300	09	03	00	Angaraes
994	090301	09	03	01	Lircay
995	090302	09	03	02	Anchonga
996	090303	09	03	03	Callanmarca
997	090304	09	03	04	Ccochaccasa
998	090305	09	03	05	Chincho
999	090306	09	03	06	Congalla
1000	090307	09	03	07	Huanca-Huanca
1001	090308	09	03	08	Huayllay Grande
1002	090309	09	03	09	Julcamarca
1003	090310	09	03	10	San Antonio de Antaparco
1004	090311	09	03	11	Santo Tomas de Pata
1005	090312	09	03	12	Secclla
1006	090400	09	04	00	Castrovirreyna
1007	090401	09	04	01	Castrovirreyna
1008	090402	09	04	02	Arma
1009	090403	09	04	03	Aurahua
1010	090404	09	04	04	Capillas
1011	090405	09	04	05	Chupamarca
1012	090406	09	04	06	Cocas
1013	090407	09	04	07	Huachos
1014	090408	09	04	08	Huamatambo
1015	090409	09	04	09	Mollepampa
1016	090410	09	04	10	San Juan
1017	090411	09	04	11	Santa Ana
1018	090412	09	04	12	Tantara
1019	090413	09	04	13	Ticrapo
1020	090500	09	05	00	Churcampa
1021	090501	09	05	01	Churcampa
1022	090502	09	05	02	Anco
1023	090503	09	05	03	Chinchihuasi
1024	090504	09	05	04	El Carmen
1025	090505	09	05	05	La Merced
1026	090506	09	05	06	Locroja
1027	090507	09	05	07	Paucarbamba
1028	090508	09	05	08	San Miguel de Mayocc
1029	090509	09	05	09	San Pedro de Coris
1030	090510	09	05	10	Pachamarca
1031	090511	09	05	11	Cosme
1032	090600	09	06	00	Huaytara
1033	090601	09	06	01	Huaytara
1034	090602	09	06	02	Ayavi
1035	090603	09	06	03	Cordova
1036	090604	09	06	04	Huayacundo Arma
1037	090605	09	06	05	Laramarca
1038	090606	09	06	06	Ocoyo
1039	090607	09	06	07	Pilpichaca
1040	090608	09	06	08	Querco
1041	090609	09	06	09	Quito-Arma
1042	090610	09	06	10	San Antonio de Cusicancha
1043	090611	09	06	11	San Francisco de Sangayaico
1044	090612	09	06	12	San Isidro
1045	090613	09	06	13	Santiago de Chocorvos
1046	090614	09	06	14	Santiago de Quirahuara
1047	090615	09	06	15	Santo Domingo de Capillas
1048	090616	09	06	16	Tambo
1049	090700	09	07	00	Tayacaja
1050	090701	09	07	01	Pampas
1051	090702	09	07	02	Acostambo
1052	090703	09	07	03	Acraquia
1053	090704	09	07	04	Ahuaycha
1054	090705	09	07	05	Colcabamba
1055	090706	09	07	06	Daniel Hernandez
1056	090707	09	07	07	Huachocolpa
1057	090709	09	07	09	Huaribamba
1058	090710	09	07	10	Ñahuimpuquio
1059	090711	09	07	11	Pazos
1060	090713	09	07	13	Quishuar
1061	090714	09	07	14	Salcabamba
1062	090715	09	07	15	Salcahuasi
1063	090716	09	07	16	San Marcos de Rocchac
1064	090717	09	07	17	Surcubamba
1065	090718	09	07	18	Tintay Puncu
1066	100000	10	00	00	Huanuco
1067	100100	10	01	00	Huanuco
1068	100101	10	01	01	Huanuco
1069	100102	10	01	02	Amarilis
1070	100103	10	01	03	Chinchao
1071	100104	10	01	04	Churubamba
1072	100105	10	01	05	Margos
1073	100106	10	01	06	Quisqui
1074	100107	10	01	07	San Francisco de Cayran
1075	100108	10	01	08	San Pedro de Chaulan
1076	100109	10	01	09	Santa Maria del Valle
1077	100110	10	01	10	Yarumayo
1078	100111	10	01	11	Pillco Marca
1079	100112	10	01	12	Yacus
1080	100200	10	02	00	Ambo
1081	100201	10	02	01	Ambo
1082	100202	10	02	02	Cayna
1083	100203	10	02	03	Colpas
1084	100204	10	02	04	Conchamarca
1085	100205	10	02	05	Huacar
1086	100206	10	02	06	San Francisco
1087	100207	10	02	07	San Rafael
1088	100208	10	02	08	Tomay Kichwa
1089	100300	10	03	00	Dos de Mayo
1090	100301	10	03	01	La Union
1091	100307	10	03	07	Chuquis
1092	100311	10	03	11	Marias
1093	100313	10	03	13	Pachas
1094	100316	10	03	16	Quivilla
1095	100317	10	03	17	Ripan
1096	100321	10	03	21	Shunqui
1097	100322	10	03	22	Sillapata
1098	100323	10	03	23	Yanas
1099	100400	10	04	00	Huacaybamba
1100	100401	10	04	01	Huacaybamba
1101	100402	10	04	02	Canchabamba
1102	100403	10	04	03	Cochabamba
1103	100404	10	04	04	Pinra
1104	100500	10	05	00	Huamalies
1105	100501	10	05	01	Llata
1106	100502	10	05	02	Arancay
1107	100503	10	05	03	Chavin de Pariarca
1108	100504	10	05	04	Jacas Grande
1109	100505	10	05	05	Jircan
1110	100506	10	05	06	Miraflores
1111	100507	10	05	07	Monzon
1112	100508	10	05	08	Punchao
1113	100509	10	05	09	Puños
1114	100510	10	05	10	Singa
1115	100511	10	05	11	Tantamayo
1116	100600	10	06	00	Leoncio Prado
1117	100601	10	06	01	Rupa-Rupa
1118	100602	10	06	02	Daniel Alomias Robles
1119	100603	10	06	03	Hermilio Valdizan
1120	100604	10	06	04	Jose Crespo y Castillo
1121	100605	10	06	05	Luyando
1122	100606	10	06	06	Mariano Damaso Beraun
1123	100700	10	07	00	Marañon
1124	100701	10	07	01	Huacrachuco
1125	100702	10	07	02	Cholon
1126	100703	10	07	03	San Buenaventura
1127	100800	10	08	00	Pachitea
1128	100801	10	08	01	Panao
1129	100802	10	08	02	Chaglla
1130	100803	10	08	03	Molino
1131	100804	10	08	04	Umari
1132	100900	10	09	00	Puerto Inca
1133	100901	10	09	01	Puerto Inca
1134	100902	10	09	02	Codo del Pozuzo
1135	100903	10	09	03	Honoria
1136	100904	10	09	04	Tournavista
1137	100905	10	09	05	Yuyapichis
1138	101000	10	10	00	Lauricocha
1139	101001	10	10	01	Jesus
1140	101002	10	10	02	Baños
1141	101003	10	10	03	Jivia
1142	101004	10	10	04	Queropalca
1143	101005	10	10	05	Rondos
1144	101006	10	10	06	San Francisco de Asis
1145	101007	10	10	07	San Miguel de Cauri
1146	101100	10	11	00	Yarowilca
1147	101101	10	11	01	Chavinillo
1148	101102	10	11	02	Cahuac
1149	101103	10	11	03	Chacabamba
1150	101104	10	11	04	Chupan
1151	101105	10	11	05	Jacas Chico
1152	101106	10	11	06	Obas
1153	101107	10	11	07	Pampamarca
1154	101108	10	11	08	Choras
1155	110000	11	00	00	Ica
1156	110100	11	01	00	Ica
1157	110101	11	01	01	Ica
1158	110102	11	01	02	La Tinguiña
1159	110103	11	01	03	Los Aquijes
1160	110104	11	01	04	Ocucaje
1161	110105	11	01	05	Pachacutec
1162	110106	11	01	06	Parcona
1163	110107	11	01	07	Pueblo Nuevo
1164	110108	11	01	08	Salas
1165	110109	11	01	09	San Jose de los Molinos
1166	110110	11	01	10	San Juan Bautista
1167	110111	11	01	11	Santiago
1168	110112	11	01	12	Subtanjalla
1169	110113	11	01	13	Tate
1170	110114	11	01	14	Yauca del Rosario
1171	110200	11	02	00	Chincha
1172	110201	11	02	01	Chincha Alta
1173	110202	11	02	02	Alto Laran
1174	110203	11	02	03	Chavin
1175	110204	11	02	04	Chincha Baja
1176	110205	11	02	05	El Carmen
1177	110206	11	02	06	Grocio Prado
1178	110207	11	02	07	Pueblo Nuevo
1179	110208	11	02	08	San Juan de Yanac
1180	110209	11	02	09	San Pedro de Huacarpana
1181	110210	11	02	10	Sunampe
1182	110211	11	02	11	Tambo de Mora
1183	110300	11	03	00	Nazca
1184	110301	11	03	01	Nazca
1185	110302	11	03	02	Changuillo
1186	110303	11	03	03	El Ingenio
1187	110304	11	03	04	Marcona
1188	110305	11	03	05	Vista Alegre
1189	110400	11	04	00	Palpa
1190	110401	11	04	01	Palpa
1191	110402	11	04	02	Llipata
1192	110403	11	04	03	Rio Grande
1193	110404	11	04	04	Santa Cruz
1194	110405	11	04	05	Tibillo
1195	110500	11	05	00	Pisco
1196	110501	11	05	01	Pisco
1197	110502	11	05	02	Huancano
1198	110503	11	05	03	Humay
1199	110504	11	05	04	Independencia
1200	110505	11	05	05	Paracas
1201	110506	11	05	06	San Andres
1202	110507	11	05	07	San Clemente
1203	110508	11	05	08	Tupac Amaru Inca
1204	120000	12	00	00	Junin
1205	120100	12	01	00	Huancayo
1206	120101	12	01	01	Huancayo
1207	120104	12	01	04	Carhuacallanga
1208	120105	12	01	05	Chacapampa
1209	120106	12	01	06	Chicche
1210	120107	12	01	07	Chilca
1211	120108	12	01	08	Chongos Alto
1212	120111	12	01	11	Chupuro
1213	120112	12	01	12	Colca
1214	120113	12	01	13	Cullhuas
1215	120114	12	01	14	El Tambo
1216	120116	12	01	16	Huacrapuquio
1217	120117	12	01	17	Hualhuas
1218	120119	12	01	19	Huancan
1219	120120	12	01	20	Huasicancha
1220	120121	12	01	21	Huayucachi
1221	120122	12	01	22	Ingenio
1222	120124	12	01	24	Pariahuanca
1223	120125	12	01	25	Pilcomayo
1224	120126	12	01	26	Pucara
1225	120127	12	01	27	Quichuay
1226	120128	12	01	28	Quilcas
1227	120129	12	01	29	San Agustin
1228	120130	12	01	30	San Jeronimo de Tunan
1229	120132	12	01	32	Saño
1230	120133	12	01	33	Sapallanga
1231	120134	12	01	34	Sicaya
1232	120135	12	01	35	Santo Domingo de Acobamba
1233	120136	12	01	36	Viques
1234	120200	12	02	00	Concepcion
1235	120201	12	02	01	Concepcion
1236	120202	12	02	02	Aco
1237	120203	12	02	03	Andamarca
1238	120204	12	02	04	Chambara
1239	120205	12	02	05	Cochas
1240	120206	12	02	06	Comas
1241	120207	12	02	07	Heroinas Toledo
1242	120208	12	02	08	Manzanares
1243	120209	12	02	09	Mariscal Castilla
1244	120210	12	02	10	Matahuasi
1245	120211	12	02	11	Mito
1246	120212	12	02	12	Nueve de Julio
1247	120213	12	02	13	Orcotuna
1248	120214	12	02	14	San Jose de Quero
1249	120215	12	02	15	Santa Rosa de Ocopa
1250	120300	12	03	00	Chanchamayo
1251	120301	12	03	01	Chanchamayo
1252	120302	12	03	02	Perene
1253	120303	12	03	03	Pichanaqui
1254	120304	12	03	04	San Luis de Shuaro
1255	120305	12	03	05	San Ramon
1256	120306	12	03	06	Vitoc
1257	120400	12	04	00	Jauja
1258	120401	12	04	01	Jauja
1259	120402	12	04	02	Acolla
1260	120403	12	04	03	Apata
1261	120404	12	04	04	Ataura
1262	120405	12	04	05	Canchayllo
1263	120406	12	04	06	Curicaca
1264	120407	12	04	07	El Mantaro
1265	120408	12	04	08	Huamali
1266	120409	12	04	09	Huaripampa
1267	120410	12	04	10	Huertas
1268	120411	12	04	11	Janjaillo
1269	120412	12	04	12	Julcan
1270	120413	12	04	13	Leonor Ordoñez
1271	120414	12	04	14	Llocllapampa
1272	120415	12	04	15	Marco
1273	120416	12	04	16	Masma
1274	120417	12	04	17	Masma Chicche
1275	120418	12	04	18	Molinos
1276	120419	12	04	19	Monobamba
1277	120420	12	04	20	Muqui
1278	120421	12	04	21	Muquiyauyo
1279	120422	12	04	22	Paca
1280	120423	12	04	23	Paccha
1281	120424	12	04	24	Pancan
1282	120425	12	04	25	Parco
1283	120426	12	04	26	Pomacancha
1284	120427	12	04	27	Ricran
1285	120428	12	04	28	San Lorenzo
1286	120429	12	04	29	San Pedro de Chunan
1287	120430	12	04	30	Sausa
1288	120431	12	04	31	Sincos
1289	120432	12	04	32	Tunan Marca
1290	120433	12	04	33	Yauli
1291	120434	12	04	34	Yauyos
1292	120500	12	05	00	Junin
1293	120501	12	05	01	Junin
1294	120502	12	05	02	Carhuamayo
1295	120503	12	05	03	Ondores
1296	120504	12	05	04	Ulcumayo
1297	120600	12	06	00	Satipo
1298	120601	12	06	01	Satipo
1299	120602	12	06	02	Coviriali
1300	120603	12	06	03	Llaylla
1301	120604	12	06	04	Mazamari
1302	120605	12	06	05	Pampa Hermosa
1303	120606	12	06	06	Pangoa
1304	120607	12	06	07	Rio Negro
1305	120608	12	06	08	Rio Tambo
1306	120699	12	06	99	Mazamari-Pangoa
1307	120700	12	07	00	Tarma
1308	120701	12	07	01	Tarma
1309	120702	12	07	02	Acobamba
1310	120703	12	07	03	Huaricolca
1311	120704	12	07	04	Huasahuasi
1312	120705	12	07	05	La Union
1313	120706	12	07	06	Palca
1314	120707	12	07	07	Palcamayo
1315	120708	12	07	08	San Pedro de Cajas
1316	120709	12	07	09	Tapo
1317	120800	12	08	00	Yauli
1318	120801	12	08	01	La Oroya
1319	120802	12	08	02	Chacapalpa
1320	120803	12	08	03	Huay-Huay
1321	120804	12	08	04	Marcapomacocha
1322	120805	12	08	05	Morococha
1323	120806	12	08	06	Paccha
1324	120807	12	08	07	Santa Barbara de Carhuacayan
1325	120808	12	08	08	Santa Rosa de Sacco
1326	120809	12	08	09	Suitucancha
1327	120810	12	08	10	Yauli
1328	120900	12	09	00	Chupaca
1329	120901	12	09	01	Chupaca
1330	120902	12	09	02	Ahuac
1331	120903	12	09	03	Chongos Bajo
1332	120904	12	09	04	Huachac
1333	120905	12	09	05	Huamancaca Chico
1334	120906	12	09	06	San Juan de Iscos
1335	120907	12	09	07	San Juan de Jarpa
1336	120908	12	09	08	3 de Diciembre
1337	120909	12	09	09	Yanacancha
1338	130000	13	00	00	La Libertad
1339	130100	13	01	00	Trujillo
1340	130101	13	01	01	Trujillo
1341	130102	13	01	02	El Porvenir
1342	130103	13	01	03	Florencia de Mora
1343	130104	13	01	04	Huanchaco
1344	130105	13	01	05	La Esperanza
1345	130106	13	01	06	Laredo
1346	130107	13	01	07	Moche
1347	130108	13	01	08	Poroto
1348	130109	13	01	09	Salaverry
1349	130110	13	01	10	Simbal
1350	130111	13	01	11	Victor Larco Herrera
1351	130200	13	02	00	Ascope
1352	130201	13	02	01	Ascope
1353	130202	13	02	02	Chicama
1354	130203	13	02	03	Chocope
1355	130204	13	02	04	Magdalena de Cao
1356	130205	13	02	05	Paijan
1357	130206	13	02	06	Razuri
1358	130207	13	02	07	Santiago de Cao
1359	130208	13	02	08	Casa Grande
1360	130300	13	03	00	Bolivar
1361	130301	13	03	01	Bolivar
1362	130302	13	03	02	Bambamarca
1363	130303	13	03	03	Condormarca
1364	130304	13	03	04	Longotea
1365	130305	13	03	05	Uchumarca
1366	130306	13	03	06	Ucuncha
1367	130400	13	04	00	Chepen
1368	130401	13	04	01	Chepen
1369	130402	13	04	02	Pacanga
1370	130403	13	04	03	Pueblo Nuevo
1371	130500	13	05	00	Julcan
1372	130501	13	05	01	Julcan
1373	130502	13	05	02	Calamarca
1374	130503	13	05	03	Carabamba
1375	130504	13	05	04	Huaso
1376	130600	13	06	00	Otuzco
1377	130601	13	06	01	Otuzco
1378	130602	13	06	02	Agallpampa
1379	130604	13	06	04	Charat
1380	130605	13	06	05	Huaranchal
1381	130606	13	06	06	La Cuesta
1382	130608	13	06	08	Mache
1383	130610	13	06	10	Paranday
1384	130611	13	06	11	Salpo
1385	130613	13	06	13	Sinsicap
1386	130614	13	06	14	Usquil
1387	130700	13	07	00	Pacasmayo
1388	130701	13	07	01	San Pedro de Lloc
1389	130702	13	07	02	Guadalupe
1390	130703	13	07	03	Jequetepeque
1391	130704	13	07	04	Pacasmayo
1392	130705	13	07	05	San Jose
1393	130800	13	08	00	Pataz
1394	130801	13	08	01	Tayabamba
1395	130802	13	08	02	Buldibuyo
1396	130803	13	08	03	Chillia
1397	130804	13	08	04	Huancaspata
1398	130805	13	08	05	Huaylillas
1399	130806	13	08	06	Huayo
1400	130807	13	08	07	Ongon
1401	130808	13	08	08	Parcoy
1402	130809	13	08	09	Pataz
1403	130810	13	08	10	Pias
1404	130811	13	08	11	Santiago de Challas
1405	130812	13	08	12	Taurija
1406	130813	13	08	13	Urpay
1407	130900	13	09	00	Sanchez Carrion
1408	130901	13	09	01	Huamachuco
1409	130902	13	09	02	Chugay
1410	130903	13	09	03	Cochorco
1411	130904	13	09	04	Curgos
1412	130905	13	09	05	Marcabal
1413	130906	13	09	06	Sanagoran
1414	130907	13	09	07	Sarin
1415	130908	13	09	08	Sartimbamba
1416	131000	13	10	00	Santiago de Chuco
1417	131001	13	10	01	Santiago de Chuco
1418	131002	13	10	02	Angasmarca
1419	131003	13	10	03	Cachicadan
1420	131004	13	10	04	Mollebamba
1421	131005	13	10	05	Mollepata
1422	131006	13	10	06	Quiruvilca
1423	131007	13	10	07	Santa Cruz de Chuca
1424	131008	13	10	08	Sitabamba
1425	131100	13	11	00	Gran Chimu
1426	131101	13	11	01	Cascas
1427	131102	13	11	02	Lucma
1428	131103	13	11	03	Marmot
1429	131104	13	11	04	Sayapullo
1430	131200	13	12	00	Viru
1431	131201	13	12	01	Viru
1432	131202	13	12	02	Chao
1433	131203	13	12	03	Guadalupito
1434	140000	14	00	00	Lambayeque
1435	140100	14	01	00	Chiclayo
1436	140101	14	01	01	Chiclayo
1437	140102	14	01	02	Chongoyape
1438	140103	14	01	03	Eten
1439	140104	14	01	04	Eten Puerto
1440	140105	14	01	05	Jose Leonardo Ortiz
1441	140106	14	01	06	La Victoria
1442	140107	14	01	07	Lagunas
1443	140108	14	01	08	Monsefu
1444	140109	14	01	09	Nueva Arica
1445	140110	14	01	10	Oyotun
1446	140111	14	01	11	Picsi
1447	140112	14	01	12	Pimentel
1448	140113	14	01	13	Reque
1449	140114	14	01	14	Santa Rosa
1450	140115	14	01	15	Saña
1451	140116	14	01	16	Cayaltí
1452	140117	14	01	17	Patapo
1453	140118	14	01	18	Pomalca
1454	140119	14	01	19	Pucalá
1455	140120	14	01	20	Tumán
1456	140200	14	02	00	Ferreñafe
1457	140201	14	02	01	Ferreñafe
1458	140202	14	02	02	Cañaris
1459	140203	14	02	03	Incahuasi
1460	140204	14	02	04	Manuel Antonio Mesones Muro
1461	140205	14	02	05	Pitipo
1462	140206	14	02	06	Pueblo Nuevo
1463	140300	14	03	00	Lambayeque
1464	140301	14	03	01	Lambayeque
1465	140302	14	03	02	Chochope
1466	140303	14	03	03	Illimo
1467	140304	14	03	04	Jayanca
1468	140305	14	03	05	Mochumi
1469	140306	14	03	06	Morrope
1470	140307	14	03	07	Motupe
1471	140308	14	03	08	Olmos
1472	140309	14	03	09	Pacora
1473	140310	14	03	10	Salas
1474	140311	14	03	11	San Jose
1475	140312	14	03	12	Tucume
1476	150000	15	00	00	Lima
1477	150100	15	01	00	Lima
1478	150101	15	01	01	Lima
1479	150102	15	01	02	Ancon
1480	150103	15	01	03	Ate
1481	150104	15	01	04	Barranco
1482	150105	15	01	05	Breña
1483	150106	15	01	06	Carabayllo
1484	150107	15	01	07	Chaclacayo
1485	150108	15	01	08	Chorrillos
1486	150109	15	01	09	Cieneguilla
1487	150110	15	01	10	Comas
1488	150111	15	01	11	El Agustino
1489	150112	15	01	12	Independencia
1490	150113	15	01	13	Jesus Maria
1491	150114	15	01	14	La Molina
1492	150115	15	01	15	La Victoria
1493	150116	15	01	16	Lince
1494	150117	15	01	17	Los Olivos
1495	150118	15	01	18	Lurigancho
1496	150119	15	01	19	Lurin
1497	150120	15	01	20	Magdalena del Mar
1498	150121	15	01	21	Pueblo Libre (Magdalena Vieja)
1499	150122	15	01	22	Miraflores
1500	150123	15	01	23	Pachacamac
1501	150124	15	01	24	Pucusana
1502	150125	15	01	25	Puente Piedra
1503	150126	15	01	26	Punta Hermosa
1504	150127	15	01	27	Punta Negra
1505	150128	15	01	28	Rimac
1506	150129	15	01	29	San Bartolo
1507	150130	15	01	30	San Borja
1508	150131	15	01	31	San Isidro
1509	150132	15	01	32	San Juan de Lurigancho
1510	150133	15	01	33	San Juan de Miraflores
1511	150134	15	01	34	San Luis
1512	150135	15	01	35	San Martin de Porres
1513	150136	15	01	36	San Miguel
1514	150137	15	01	37	Santa Anita
1515	150138	15	01	38	Santa Maria del Mar
1516	150139	15	01	39	Santa Rosa
1517	150140	15	01	40	Santiago de Surco
1518	150141	15	01	41	Surquillo
1519	150142	15	01	42	Villa El Salvador
1520	150143	15	01	43	Villa Maria del Triunfo
1521	150200	15	02	00	Barranca
1522	150201	15	02	01	Barranca
1523	150202	15	02	02	Paramonga
1524	150203	15	02	03	Pativilca
1525	150204	15	02	04	Supe
1526	150205	15	02	05	Supe Puerto
1527	150300	15	03	00	Cajatambo
1528	150301	15	03	01	Cajatambo
1529	150302	15	03	02	Copa
1530	150303	15	03	03	Gorgor
1531	150304	15	03	04	Huancapon
1532	150305	15	03	05	Manas
1533	150400	15	04	00	Canta
1534	150401	15	04	01	Canta
1535	150402	15	04	02	Arahuay
1536	150403	15	04	03	Huamantanga
1537	150404	15	04	04	Huaros
1538	150405	15	04	05	Lachaqui
1539	150406	15	04	06	San Buenaventura
1540	150407	15	04	07	Santa Rosa de Quives
1541	150500	15	05	00	Cañete
1542	150501	15	05	01	San Vicente de Cañete
1543	150502	15	05	02	Asia
1544	150503	15	05	03	Calango
1545	150504	15	05	04	Cerro Azul
1546	150505	15	05	05	Chilca
1547	150506	15	05	06	Coayllo
1548	150507	15	05	07	Imperial
1549	150508	15	05	08	Lunahuana
1550	150509	15	05	09	Mala
1551	150510	15	05	10	Nuevo Imperial
1552	150511	15	05	11	Pacaran
1553	150512	15	05	12	Quilmana
1554	150513	15	05	13	San Antonio
1555	150514	15	05	14	San Luis
1556	150515	15	05	15	Santa Cruz de Flores
1557	150516	15	05	16	Zuñiga
1558	150600	15	06	00	Huaral
1559	150601	15	06	01	Huaral
1560	150602	15	06	02	Atavillos Alto
1561	150603	15	06	03	Atavillos Bajo
1562	150604	15	06	04	Aucallama
1563	150605	15	06	05	Chancay
1564	150606	15	06	06	Ihuari
1565	150607	15	06	07	Lampian
1566	150608	15	06	08	Pacaraos
1567	150609	15	06	09	San Miguel de Acos
1568	150610	15	06	10	Santa Cruz de Andamarca
1569	150611	15	06	11	Sumbilca
1570	150612	15	06	12	Veintisiete de Noviembre
1571	150700	15	07	00	Huarochiri
1572	150701	15	07	01	Matucana
1573	150702	15	07	02	Antioquia
1574	150703	15	07	03	Callahuanca
1575	150704	15	07	04	Carampoma
1576	150705	15	07	05	Chicla
1577	150706	15	07	06	Cuenca
1578	150707	15	07	07	Huachupampa
1579	150708	15	07	08	Huanza
1580	150709	15	07	09	Huarochiri
1581	150710	15	07	10	Lahuaytambo
1582	150711	15	07	11	Langa
1583	150712	15	07	12	Laraos
1584	150713	15	07	13	Mariatana
1585	150714	15	07	14	Ricardo Palma
1586	150715	15	07	15	San Andres de Tupicocha
1587	150716	15	07	16	San Antonio
1588	150717	15	07	17	San Bartolome
1589	150718	15	07	18	San Damian
1590	150719	15	07	19	San Juan de Iris
1591	150720	15	07	20	San Juan de Tantaranche
1592	150721	15	07	21	San Lorenzo de Quinti
1593	150722	15	07	22	San Mateo
1594	150723	15	07	23	San Mateo de Otao
1595	150724	15	07	24	San Pedro de Casta
1596	150725	15	07	25	San Pedro de Huancayre
1597	150726	15	07	26	Sangallaya
1598	150727	15	07	27	Santa Cruz de Cocachacra
1599	150728	15	07	28	Santa Eulalia
1600	150729	15	07	29	Santiago de Anchucaya
1601	150730	15	07	30	Santiago de Tuna
1602	150731	15	07	31	Santo Domingo de los Olleros
1603	150732	15	07	32	Surco
1604	150800	15	08	00	Huaura
1605	150801	15	08	01	Huacho
1606	150802	15	08	02	Ambar
1607	150803	15	08	03	Caleta de Carquin
1608	150804	15	08	04	Checras
1609	150805	15	08	05	Hualmay
1610	150806	15	08	06	Huaura
1611	150807	15	08	07	Leoncio Prado
1612	150808	15	08	08	Paccho
1613	150809	15	08	09	Santa Leonor
1614	150810	15	08	10	Santa Maria
1615	150811	15	08	11	Sayan
1616	150812	15	08	12	Vegueta
1617	150900	15	09	00	Oyon
1618	150901	15	09	01	Oyon
1619	150902	15	09	02	Andajes
1620	150903	15	09	03	Caujul
1621	150904	15	09	04	Cochamarca
1622	150905	15	09	05	Navan
1623	150906	15	09	06	Pachangara
1624	151000	15	10	00	Yauyos
1625	151001	15	10	01	Yauyos
1626	151002	15	10	02	Alis
1627	151003	15	10	03	Ayauca
1628	151004	15	10	04	Ayaviri
1629	151005	15	10	05	Azangaro
1630	151006	15	10	06	Cacra
1631	151007	15	10	07	Carania
1632	151008	15	10	08	Catahuasi
1633	151009	15	10	09	Chocos
1634	151010	15	10	10	Cochas
1635	151011	15	10	11	Colonia
1636	151012	15	10	12	Hongos
1637	151013	15	10	13	Huampara
1638	151014	15	10	14	Huancaya
1639	151015	15	10	15	Huangascar
1640	151016	15	10	16	Huantan
1641	151017	15	10	17	Huañec
1642	151018	15	10	18	Laraos
1643	151019	15	10	19	Lincha
1644	151020	15	10	20	Madean
1645	151021	15	10	21	Miraflores
1646	151022	15	10	22	Omas
1647	151023	15	10	23	Putinza
1648	151024	15	10	24	Quinches
1649	151025	15	10	25	Quinocay
1650	151026	15	10	26	San Joaquin
1651	151027	15	10	27	San Pedro de Pilas
1652	151028	15	10	28	Tanta
1653	151029	15	10	29	Tauripampa
1654	151030	15	10	30	Tomas
1655	151031	15	10	31	Tupe
1656	151032	15	10	32	Viñac
1657	151033	15	10	33	Vitis
1658	160000	16	00	00	Loreto
1659	160100	16	01	00	Maynas
1660	160101	16	01	01	Iquitos
1661	160102	16	01	02	Alto Nanay
1662	160103	16	01	03	Fernando Lores
1663	160104	16	01	04	Indiana
1664	160105	16	01	05	Las Amazonas
1665	160106	16	01	06	Mazan
1666	160107	16	01	07	Napo
1667	160108	16	01	08	Punchana
1668	160109	16	01	09	Putumayo
1669	160110	16	01	10	Torres Causana
1670	160112	16	01	12	Belén
1671	160113	16	01	13	San Juan Bautista
1672	160114	16	01	14	Teniente Manuel Clavero
1673	160200	16	02	00	Alto Amazonas
1674	160201	16	02	01	Yurimaguas
1675	160202	16	02	02	Balsapuerto
1676	160205	16	02	05	Jeberos
1677	160206	16	02	06	Lagunas
1678	160210	16	02	10	Santa Cruz
1679	160211	16	02	11	Teniente Cesar Lopez Rojas
1680	160300	16	03	00	Loreto
1681	160301	16	03	01	Nauta
1682	160302	16	03	02	Parinari
1683	160303	16	03	03	Tigre
1684	160304	16	03	04	Trompeteros
1685	160305	16	03	05	Urarinas
1686	160400	16	04	00	Mariscal Ramon Castilla
1687	160401	16	04	01	Ramon Castilla
1688	160402	16	04	02	Pebas
1689	160403	16	04	03	Yavari
1690	160404	16	04	04	San Pablo
1691	160500	16	05	00	Requena
1692	160501	16	05	01	Requena
1693	160502	16	05	02	Alto Tapiche
1694	160503	16	05	03	Capelo
1695	160504	16	05	04	Emilio San Martin
1696	160505	16	05	05	Maquia
1697	160506	16	05	06	Puinahua
1698	160507	16	05	07	Saquena
1699	160508	16	05	08	Soplin
1700	160509	16	05	09	Tapiche
1701	160510	16	05	10	Jenaro Herrera
1702	160511	16	05	11	Yaquerana
1703	160600	16	06	00	Ucayali
1704	160601	16	06	01	Contamana
1705	160602	16	06	02	Inahuaya
1706	160603	16	06	03	Padre Marquez
1707	160604	16	06	04	Pampa Hermosa
1708	160605	16	06	05	Sarayacu
1709	160606	16	06	06	Vargas Guerra
1710	160700	16	07	00	Datem del Marañón
1711	160701	16	07	01	Barranca
1712	160702	16	07	02	Cahuapanas
1713	160703	16	07	03	Manseriche
1714	160704	16	07	04	Morona
1715	160705	16	07	05	Pastaza
1716	160706	16	07	06	Andoas
1717	160800	16	08	00	Putumayo
1718	160801	16	08	01	Putumayo
1719	160802	16	08	02	Rosa Panduro
1720	160803	16	08	03	Teniente Manuel Clavero
1721	160804	16	08	04	Yaguas
1722	170000	17	00	00	Madre de Dios
1723	170100	17	01	00	Tambopata
1724	170101	17	01	01	Tambopata
1725	170102	17	01	02	Inambari
1726	170103	17	01	03	Las Piedras
1727	170104	17	01	04	Laberinto
1728	170200	17	02	00	Manu
1729	170201	17	02	01	Manu
1730	170202	17	02	02	Fitzcarrald
1731	170203	17	02	03	Madre de Dios
1732	170204	17	02	04	Huepetuhe
1733	170300	17	03	00	Tahuamanu
1734	170301	17	03	01	Iñapari
1735	170302	17	03	02	Iberia
1736	170303	17	03	03	Tahuamanu
1737	180000	18	00	00	Moquegua
1738	180100	18	01	00	Mariscal Nieto
1739	180101	18	01	01	Moquegua
1740	180102	18	01	02	Carumas
1741	180103	18	01	03	Cuchumbaya
1742	180104	18	01	04	Samegua
1743	180105	18	01	05	San Cristobal
1744	180106	18	01	06	Torata
1745	180200	18	02	00	General Sanchez Cerro
1746	180201	18	02	01	Omate
1747	180202	18	02	02	Chojata
1748	180203	18	02	03	Coalaque
1749	180204	18	02	04	Ichuña
1750	180205	18	02	05	La Capilla
1751	180206	18	02	06	Lloque
1752	180207	18	02	07	Matalaque
1753	180208	18	02	08	Puquina
1754	180209	18	02	09	Quinistaquillas
1755	180210	18	02	10	Ubinas
1756	180211	18	02	11	Yunga
1757	180300	18	03	00	Ilo
1758	180301	18	03	01	Ilo
1759	180302	18	03	02	El Algarrobal
1760	180303	18	03	03	Pacocha
1761	190000	19	00	00	Pasco
1762	190100	19	01	00	Pasco
1763	190101	19	01	01	Chaupimarca
1764	190102	19	01	02	Huachon
1765	190103	19	01	03	Huariaca
1766	190104	19	01	04	Huayllay
1767	190105	19	01	05	Ninacaca
1768	190106	19	01	06	Pallanchacra
1769	190107	19	01	07	Paucartambo
1770	190108	19	01	08	San Fco. de Asís de Yarusyacán
1771	190109	19	01	09	Simon Bolivar
1772	190110	19	01	10	Ticlacayan
1773	190111	19	01	11	Tinyahuarco
1774	190112	19	01	12	Vicco
1775	190113	19	01	13	Yanacancha
1776	190200	19	02	00	Daniel Alcides Carrion
1777	190201	19	02	01	Yanahuanca
1778	190202	19	02	02	Chacayan
1779	190203	19	02	03	Goyllarisquizga
1780	190204	19	02	04	Paucar
1781	190205	19	02	05	San Pedro de Pillao
1782	190206	19	02	06	Santa Ana de Tusi
1783	190207	19	02	07	Tapuc
1784	190208	19	02	08	Vilcabamba
1785	190300	19	03	00	Oxapampa
1786	190301	19	03	01	Oxapampa
1787	190302	19	03	02	Chontabamba
1788	190303	19	03	03	Huancabamba
1789	190304	19	03	04	Palcazu
1790	190305	19	03	05	Pozuzo
1791	190306	19	03	06	Puerto Bermudez
1792	190307	19	03	07	Villa Rica
1793	190308	19	03	08	Constitucion
1794	200000	20	00	00	Piura
1795	200100	20	01	00	Piura
1796	200101	20	01	01	Piura
1797	200104	20	01	04	Castilla
1798	200105	20	01	05	Catacaos
1799	200107	20	01	07	Cura Mori
1800	200108	20	01	08	El Tallan
1801	200109	20	01	09	La Arena
1802	200110	20	01	10	La Union
1803	200111	20	01	11	Las Lomas
1804	200114	20	01	14	Tambo Grande
1805	200115	20	01	15	Veintiséis de Octubre
1806	200200	20	02	00	Ayabaca
1807	200201	20	02	01	Ayabaca
1808	200202	20	02	02	Frias
1809	200203	20	02	03	Jilili
1810	200204	20	02	04	Lagunas
1811	200205	20	02	05	Montero
1812	200206	20	02	06	Pacaipampa
1813	200207	20	02	07	Paimas
1814	200208	20	02	08	Sapillica
1815	200209	20	02	09	Sicchez
1816	200210	20	02	10	Suyo
1817	200300	20	03	00	Huancabamba
1818	200301	20	03	01	Huancabamba
1819	200302	20	03	02	Canchaque
1820	200303	20	03	03	El Carmen de la Frontera
1821	200304	20	03	04	Huarmaca
1822	200305	20	03	05	Lalaquiz
1823	200306	20	03	06	San Miguel de El Faique
1824	200307	20	03	07	Sondor
1825	200308	20	03	08	Sondorillo
1826	200400	20	04	00	Morropon
1827	200401	20	04	01	Chulucanas
1828	200402	20	04	02	Buenos Aires
1829	200403	20	04	03	Chalaco
1830	200404	20	04	04	La Matanza
1831	200405	20	04	05	Morropon
1832	200406	20	04	06	Salitral
1833	200407	20	04	07	San Juan de Bigote
1834	200408	20	04	08	Santa Catalina de Mossa
1835	200409	20	04	09	Santo Domingo
1836	200410	20	04	10	Yamango
1837	200500	20	05	00	Paita
1838	200501	20	05	01	Paita
1839	200502	20	05	02	Amotape
1840	200503	20	05	03	Arenal
1841	200504	20	05	04	Colan
1842	200505	20	05	05	La Huaca
1843	200506	20	05	06	Tamarindo
1844	200507	20	05	07	Vichayal
1845	200600	20	06	00	Sullana
1846	200601	20	06	01	Sullana
1847	200602	20	06	02	Bellavista
1848	200603	20	06	03	Ignacio Escudero
1849	200604	20	06	04	Lancones
1850	200605	20	06	05	Marcavelica
1851	200606	20	06	06	Miguel Checa
1852	200607	20	06	07	Querecotillo
1853	200608	20	06	08	Salitral
1854	200700	20	07	00	Talara
1855	200701	20	07	01	Pariñas
1856	200702	20	07	02	El Alto
1857	200703	20	07	03	La Brea
1858	200704	20	07	04	Lobitos
1859	200705	20	07	05	Los Organos
1860	200706	20	07	06	Mancora
1861	200800	20	08	00	Sechura
1862	200801	20	08	01	Sechura
1863	200802	20	08	02	Bellavista de la Union
1864	200803	20	08	03	Bernal
1865	200804	20	08	04	Cristo Nos Valga
1866	200805	20	08	05	Vice
1867	200806	20	08	06	Rinconada Llicuar
1868	210000	21	00	00	Puno
1869	210100	21	01	00	Puno
1870	210101	21	01	01	Puno
1871	210102	21	01	02	Acora
1872	210103	21	01	03	Amantani
1873	210104	21	01	04	Atuncolla
1874	210105	21	01	05	Capachica
1875	210106	21	01	06	Chucuito
1876	210107	21	01	07	Coata
1877	210108	21	01	08	Huata
1878	210109	21	01	09	Mañazo
1879	210110	21	01	10	Paucarcolla
1880	210111	21	01	11	Pichacani
1881	210112	21	01	12	Plateria
1882	210113	21	01	13	San Antonio
1883	210114	21	01	14	Tiquillaca
1884	210115	21	01	15	Vilque
1885	210200	21	02	00	Azangaro
1886	210201	21	02	01	Azangaro
1887	210202	21	02	02	Achaya
1888	210203	21	02	03	Arapa
1889	210204	21	02	04	Asillo
1890	210205	21	02	05	Caminaca
1891	210206	21	02	06	Chupa
1892	210207	21	02	07	Jose Domingo Choquehuanca
1893	210208	21	02	08	Muñani
1894	210209	21	02	09	Potoni
1895	210210	21	02	10	Saman
1896	210211	21	02	11	San Anton
1897	210212	21	02	12	San Jose
1898	210213	21	02	13	San Juan de Salinas
1899	210214	21	02	14	Santiago de Pupuja
1900	210215	21	02	15	Tirapata
1901	210300	21	03	00	Carabaya
1902	210301	21	03	01	Macusani
1903	210302	21	03	02	Ajoyani
1904	210303	21	03	03	Ayapata
1905	210304	21	03	04	Coasa
1906	210305	21	03	05	Corani
1907	210306	21	03	06	Crucero
1908	210307	21	03	07	Ituata
1909	210308	21	03	08	Ollachea
1910	210309	21	03	09	San Gaban
1911	210310	21	03	10	Usicayos
1912	210400	21	04	00	Chucuito
1913	210401	21	04	01	Juli
1914	210402	21	04	02	Desaguadero
1915	210403	21	04	03	Huacullani
1916	210404	21	04	04	Kelluyo
1917	210405	21	04	05	Pisacoma
1918	210406	21	04	06	Pomata
1919	210407	21	04	07	Zepita
1920	210500	21	05	00	El Collao
1921	210501	21	05	01	Ilave
1922	210502	21	05	02	Capaso
1923	210503	21	05	03	Pilcuyo
1924	210504	21	05	04	Santa Rosa
1925	210505	21	05	05	Conduriri
1926	210600	21	06	00	Huancane
1927	210601	21	06	01	Huancane
1928	210602	21	06	02	Cojata
1929	210603	21	06	03	Huatasani
1930	210604	21	06	04	Inchupalla
1931	210605	21	06	05	Pusi
1932	210606	21	06	06	Rosaspata
1933	210607	21	06	07	Taraco
1934	210608	21	06	08	Vilque Chico
1935	210700	21	07	00	Lampa
1936	210701	21	07	01	Lampa
1937	210702	21	07	02	Cabanilla
1938	210703	21	07	03	Calapuja
1939	210704	21	07	04	Nicasio
1940	210705	21	07	05	Ocuviri
1941	210706	21	07	06	Palca
1942	210707	21	07	07	Paratia
1943	210708	21	07	08	Pucara
1944	210709	21	07	09	Santa Lucia
1945	210710	21	07	10	Vilavila
1946	210800	21	08	00	Melgar
1947	210801	21	08	01	Ayaviri
1948	210802	21	08	02	Antauta
1949	210803	21	08	03	Cupi
1950	210804	21	08	04	Llalli
1951	210805	21	08	05	Macari
1952	210806	21	08	06	Nuñoa
1953	210807	21	08	07	Orurillo
1954	210808	21	08	08	Santa Rosa
1955	210809	21	08	09	Umachiri
1956	210900	21	09	00	Moho
1957	210901	21	09	01	Moho
1958	210902	21	09	02	Conima
1959	210903	21	09	03	Huayrapata
1960	210904	21	09	04	Tilali
1961	211000	21	10	00	San Antonio de Putina
1962	211001	21	10	01	Putina
1963	211002	21	10	02	Ananea
1964	211003	21	10	03	Pedro Vilca Apaza
1965	211004	21	10	04	Quilcapuncu
1966	211005	21	10	05	Sina
1967	211100	21	11	00	San Roman
1968	211101	21	11	01	Juliaca
1969	211102	21	11	02	Cabana
1970	211103	21	11	03	Cabanillas
1971	211104	21	11	04	Caracoto
1972	211200	21	12	00	Sandia
1973	211201	21	12	01	Sandia
1974	211202	21	12	02	Cuyocuyo
1975	211203	21	12	03	Limbani
1976	211204	21	12	04	Patambuco
1977	211205	21	12	05	Phara
1978	211206	21	12	06	Quiaca
1979	211207	21	12	07	San Juan del Oro
1980	211208	21	12	08	Yanahuaya
1981	211209	21	12	09	Alto Inambari
1982	211210	21	12	10	San Pedro de Putina Punco
1983	211300	21	13	00	Yunguyo
1984	211301	21	13	01	Yunguyo
1985	211302	21	13	02	Anapia
1986	211303	21	13	03	Copani
1987	211304	21	13	04	Cuturapi
1988	211305	21	13	05	Ollaraya
1989	211306	21	13	06	Tinicachi
1990	211307	21	13	07	Unicachi
1991	220000	22	00	00	San Martin
1992	220100	22	01	00	Moyobamba
1993	220101	22	01	01	Moyobamba
1994	220102	22	01	02	Calzada
1995	220103	22	01	03	Habana
1996	220104	22	01	04	Jepelacio
1997	220105	22	01	05	Soritor
1998	220106	22	01	06	Yantalo
1999	220200	22	02	00	Bellavista
2000	220201	22	02	01	Bellavista
2001	220202	22	02	02	Alto Biavo
2002	220203	22	02	03	Bajo Biavo
2003	220204	22	02	04	Huallaga
2004	220205	22	02	05	San Pablo
2005	220206	22	02	06	San Rafael
2006	220300	22	03	00	El Dorado
2007	220301	22	03	01	San Jose de Sisa
2008	220302	22	03	02	Agua Blanca
2009	220303	22	03	03	San Martin
2010	220304	22	03	04	Santa Rosa
2011	220305	22	03	05	Shatoja
2012	220400	22	04	00	Huallaga
2013	220401	22	04	01	Saposoa
2014	220402	22	04	02	Alto Saposoa
2015	220403	22	04	03	El Eslabon
2016	220404	22	04	04	Piscoyacu
2017	220405	22	04	05	Sacanche
2018	220406	22	04	06	Tingo de Saposoa
2019	220500	22	05	00	Lamas
2020	220501	22	05	01	Lamas
2021	220502	22	05	02	Alonso de Alvarado
2022	220503	22	05	03	Barranquita
2023	220504	22	05	04	Caynarachi
2024	220505	22	05	05	Cuñumbuqui
2025	220506	22	05	06	Pinto Recodo
2026	220507	22	05	07	Rumisapa
2027	220508	22	05	08	San Roque de Cumbaza
2028	220509	22	05	09	Shanao
2029	220510	22	05	10	Tabalosos
2030	220511	22	05	11	Zapatero
2031	220600	22	06	00	Mariscal Caceres
2032	220601	22	06	01	Juanjui
2033	220602	22	06	02	Campanilla
2034	220603	22	06	03	Huicungo
2035	220604	22	06	04	Pachiza
2036	220605	22	06	05	Pajarillo
2037	220700	22	07	00	Picota
2038	220701	22	07	01	Picota
2039	220702	22	07	02	Buenos Aires
2040	220703	22	07	03	Caspisapa
2041	220704	22	07	04	Pilluana
2042	220705	22	07	05	Pucacaca
2043	220706	22	07	06	San Cristobal
2044	220707	22	07	07	San Hilarion
2045	220708	22	07	08	Shamboyacu
2046	220709	22	07	09	Tingo de Ponasa
2047	220710	22	07	10	Tres Unidos
2048	220800	22	08	00	Rioja
2049	220801	22	08	01	Rioja
2050	220802	22	08	02	Awajun
2051	220803	22	08	03	Elias Soplin Vargas
2052	220804	22	08	04	Nueva Cajamarca
2053	220805	22	08	05	Pardo Miguel
2054	220806	22	08	06	Posic
2055	220807	22	08	07	San Fernando
2056	220808	22	08	08	Yorongos
2057	220809	22	08	09	Yuracyacu
2058	220900	22	09	00	San Martin
2059	220901	22	09	01	Tarapoto
2060	220902	22	09	02	Alberto Leveau
2061	220903	22	09	03	Cacatachi
2062	220904	22	09	04	Chazuta
2063	220905	22	09	05	Chipurana
2064	220906	22	09	06	El Porvenir
2065	220907	22	09	07	Huimbayoc
2066	220908	22	09	08	Juan Guerra
2067	220909	22	09	09	La Banda de Shilcayo
2068	220910	22	09	10	Morales
2069	220911	22	09	11	Papaplaya
2070	220912	22	09	12	San Antonio
2071	220913	22	09	13	Sauce
2072	220914	22	09	14	Shapaja
2073	221000	22	10	00	Tocache
2074	221001	22	10	01	Tocache
2075	221002	22	10	02	Nuevo Progreso
2076	221003	22	10	03	Polvora
2077	221004	22	10	04	Shunte
2078	221005	22	10	05	Uchiza
2079	230000	23	00	00	Tacna
2080	230100	23	01	00	Tacna
2081	230101	23	01	01	Tacna
2082	230102	23	01	02	Alto de la Alianza
2083	230103	23	01	03	Calana
2084	230104	23	01	04	Ciudad Nueva
2085	230105	23	01	05	Inclan
2086	230106	23	01	06	Pachia
2087	230107	23	01	07	Palca
2088	230108	23	01	08	Pocollay
2089	230109	23	01	09	Sama
2090	230110	23	01	10	Coronel Gregorio Albarracín L
2091	230200	23	02	00	Candarave
2092	230201	23	02	01	Candarave
2093	230202	23	02	02	Cairani
2094	230203	23	02	03	Camilaca
2095	230204	23	02	04	Curibaya
2096	230205	23	02	05	Huanuara
2097	230206	23	02	06	Quilahuani
2098	230300	23	03	00	Jorge Basadre
2099	230301	23	03	01	Locumba
2100	230302	23	03	02	Ilabaya
2101	230303	23	03	03	Ite
2102	230400	23	04	00	Tarata
2103	230401	23	04	01	Tarata
2104	230402	23	04	02	Chucatamani
2105	230403	23	04	03	Estique
2106	230404	23	04	04	Estique-Pampa
2107	230405	23	04	05	Sitajara
2108	230406	23	04	06	Susapaya
2109	230407	23	04	07	Tarucachi
2110	230408	23	04	08	Ticaco
2111	240000	24	00	00	Tumbes
2112	240100	24	01	00	Tumbes
2113	240101	24	01	01	Tumbes
2114	240102	24	01	02	Corrales
2115	240103	24	01	03	La Cruz
2116	240104	24	01	04	Pampas de Hospital
2117	240105	24	01	05	San Jacinto
2118	240106	24	01	06	San Juan de la Virgen
2119	240200	24	02	00	Contralmirante Villar
2120	240201	24	02	01	Zorritos
2121	240202	24	02	02	Casitas
2122	240203	24	02	03	Canoas de Punta Sal
2123	240300	24	03	00	Zarumilla
2124	240301	24	03	01	Zarumilla
2125	240302	24	03	02	Aguas Verdes
2126	240303	24	03	03	Matapalo
2127	240304	24	03	04	Papayal
2128	250000	25	00	00	Ucayali
2129	250100	25	01	00	Coronel Portillo
2130	250101	25	01	01	Callaria
2131	250102	25	01	02	Campoverde
2132	250103	25	01	03	Iparia
2133	250104	25	01	04	Masisea
2134	250105	25	01	05	Yarinacocha
2135	250106	25	01	06	Nueva Requena
2136	250107	25	01	07	Manantay
2137	250200	25	02	00	Atalaya
2138	250201	25	02	01	Raymondi
2139	250202	25	02	02	Sepahua
2140	250203	25	02	03	Tahuania
2141	250204	25	02	04	Yurua
2142	250300	25	03	00	Padre Abad
2143	250301	25	03	01	Padre Abad
2144	250302	25	03	02	Irazola
2145	250303	25	03	03	Curimana
2146	250400	25	04	00	Purus
2147	250401	25	04	01	Purus
2148	990000	99	00	00	Extranjero
2149	999900	99	99	00	Extranjero
2150	999999	99	99	99	Extranjero
\.


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 199
-- Name: ubigeo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ubigeo_id_seq', 2150, true);


--
-- TOC entry 2245 (class 0 OID 22547)
-- Dependencies: 184
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, name, email, password, remember_token, created_at, updated_at) FROM stdin;
2	Juan Paredes	jufran20@gmail.com	$2y$10$knpiW8mQzFn6B4Eaqm3Ac.iDrXFyp1UrjonUULChcVzko9uJHeRQS	\N	2018-03-30 00:20:31	2018-03-30 00:20:31
1	Luis Casiano	jcasiano20@gmail.com	$2y$10$i52Bsel6L0DdL7Wo4ps.P.JVlLcq84s2lM5a3PM.qDowEvLYJqhuO	slV9kxa61NB9v7GUtTMIdTd1hBnUYjpNhjLPRQveZSYBe1p2vFQlaQ4lpJW1	2018-03-29 23:03:37	2018-03-29 23:03:37
\.


--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 2, true);


--
-- TOC entry 2095 (class 2606 OID 22577)
-- Name: estado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pk PRIMARY KEY (id);


--
-- TOC entry 2097 (class 2606 OID 22579)
-- Name: estado_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_unique UNIQUE (codigo);


--
-- TOC entry 2109 (class 2606 OID 22650)
-- Name: evento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT evento_pk PRIMARY KEY (id);


--
-- TOC entry 2099 (class 2606 OID 22765)
-- Name: generica_codigo_uq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY generica
    ADD CONSTRAINT generica_codigo_uq UNIQUE (tipo, codigo);


--
-- TOC entry 2101 (class 2606 OID 22607)
-- Name: generica_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY generica
    ADD CONSTRAINT generica_pk PRIMARY KEY (id);


--
-- TOC entry 2103 (class 2606 OID 22622)
-- Name: institucion_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institucion
    ADD CONSTRAINT institucion_pk PRIMARY KEY (id);


--
-- TOC entry 2105 (class 2606 OID 22624)
-- Name: institucion_uq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institucion
    ADD CONSTRAINT institucion_uq UNIQUE (codigo_modular);


--
-- TOC entry 2088 (class 2606 OID 22544)
-- Name: migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2111 (class 2606 OID 22682)
-- Name: persona_documento_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT persona_documento_unique UNIQUE (tipo_doc, num_doc);


--
-- TOC entry 2113 (class 2606 OID 22684)
-- Name: persona_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT persona_email_unique UNIQUE (email);


--
-- TOC entry 2117 (class 2606 OID 22706)
-- Name: persona_evento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona_evento
    ADD CONSTRAINT persona_evento_pk PRIMARY KEY (id_persona, id_evento);


--
-- TOC entry 2115 (class 2606 OID 22680)
-- Name: persona_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT persona_pk PRIMARY KEY (id);


--
-- TOC entry 2121 (class 2606 OID 22773)
-- Name: personal_institucion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personal_institucion
    ADD CONSTRAINT personal_institucion_pkey PRIMARY KEY (id);


--
-- TOC entry 2107 (class 2606 OID 22637)
-- Name: tipo_evento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipo_evento
    ADD CONSTRAINT tipo_evento_pk PRIMARY KEY (id);


--
-- TOC entry 2119 (class 2606 OID 22736)
-- Name: ubigeo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ubigeo
    ADD CONSTRAINT ubigeo_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 22557)
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2092 (class 2606 OID 22555)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2093 (class 1259 OID 22564)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- TOC entry 2124 (class 2606 OID 22685)
-- Name: estado_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT estado_fk FOREIGN KEY (id_estado) REFERENCES estado(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2126 (class 2606 OID 22707)
-- Name: evento_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona_evento
    ADD CONSTRAINT evento_fk FOREIGN KEY (id_evento) REFERENCES evento(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2122 (class 2606 OID 22651)
-- Name: institucion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT institucion_fk FOREIGN KEY (id_institucion) REFERENCES institucion(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2125 (class 2606 OID 22690)
-- Name: institucion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona
    ADD CONSTRAINT institucion_fk FOREIGN KEY (id_institucion) REFERENCES institucion(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2127 (class 2606 OID 22712)
-- Name: persona_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY persona_evento
    ADD CONSTRAINT persona_fk FOREIGN KEY (id_persona) REFERENCES persona(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2123 (class 2606 OID 22656)
-- Name: tipo_evento_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY evento
    ADD CONSTRAINT tipo_evento_fk FOREIGN KEY (id_tipo_evento) REFERENCES tipo_evento(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-03-30 05:31:18

--
-- PostgreSQL database dump complete
--

